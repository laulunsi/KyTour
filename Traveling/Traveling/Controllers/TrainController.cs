﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Model;
using Travel_Service;

namespace Traveling.Controllers
{
    public class TrainController : Controller
    {
        //
        // GET: /Train/
        TrainService ser = new TrainService();
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 显示车次列表
        /// </summary>
        /// <returns></returns>
        public JsonResult Showtrainnumbertype()
        {
            List<trainnumbertype> list = ser.showtrainnumbertype();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 显示座位列表
        /// </summary>
        /// <returns></returns>
        public JsonResult showseattype()
        {
            List<seattype> list = ser.showseattype();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 显示时段信息
        /// </summary>
        /// <returns></returns>
        public JsonResult showtimeframe()
        {
            List<timeframe> list = ser.showtimeframe();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult  showtrainnumber()
        {
            List<trainnumber> list = ser.showtrainnumber();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
