﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
using Travel_Model;

namespace Traveling.Controllers
{
    public class NavController : Controller
    {
        //
        // GET: /Nav/
        NavService service = new NavService();
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 显示导航栏
        /// </summary>
        /// <returns></returns>
        public ActionResult shownav()
        {
            List<nav> list = service.navshow();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 显示轮播图片
        /// </summary>
        /// <returns></returns>
        public ActionResult imgshow()
        {
            List<advertisement> list = service.showimg();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
