﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
using Travel_Model;
using System.Data;
namespace Traveling.Controllers
{
    public class HolidayController : Controller
    {
        //
        // GET: /Holiday/
        HolidayService bll = new HolidayService();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult Holiday_Shows()
        {
            List<holiday> list = bll.Holiday_Show();
            return Json(list,JsonRequestBehavior.AllowGet);
        }

    }
}
