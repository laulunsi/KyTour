﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
using Travel_Model;

namespace Traveling.Controllers
{
    public class HotelController : Controller
    {
        //
        // GET: /Hotel/
        HotelService service = new HotelService();
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult hotel()
        {
            List<hotel> list = service.hotelshow();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}
