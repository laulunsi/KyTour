﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
namespace Traveling.Controllers
{
    public class AirplaneController : Controller
    {
        //
        // GET: /Airplane/
        AirplaneService bll = new AirplaneService();

        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 机场显示
        /// </summary>
        /// <returns></returns>
        public ActionResult Airplane_Shwo()
        {
            return Json(bll.AirlineShwo(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 机票显示
        /// </summary>
        /// <returns></returns>
        public ActionResult airticket_Show()
        {
            return Json(bll.airticketShow(), JsonRequestBehavior.AllowGet);
        }

    }
}
