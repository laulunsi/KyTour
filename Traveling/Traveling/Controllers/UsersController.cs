﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
using Travel_Model;
using System.Data;
using System.Security.Cryptography;
using System.Text;

namespace Traveling.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/
        UsersService service = new UsersService();
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 判断数据库中是否有数据
        /// </summary>
        /// <param name="uname"></param>
        /// <param name="upwd"></param>
        /// <returns></returns>
        public int LisetUser(string uname,string upwd)
        {
            byte[] result = Encoding.Default.GetBytes(upwd.Trim());
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);

            string pwds = BitConverter.ToString(output);
            DataSet ds = service.ListCount(uname, pwds);
           
            if(ds.Tables[0].Rows.Count>0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 显示用户类型
        /// </summary>
        /// <returns></returns>
        public ActionResult showtype()
        {
            DataSet ds= service.showtype();
            DataTable dt = ds.Tables[0];

            List<user_type> list = new List<user_type>();
            foreach (DataRow item in dt.Rows)
            {
                user_type us = new user_type();
                us.usertype = Convert.ToInt32(item["usertype"]);
                us.typename = item["typename"].ToString();
                list.Add(us);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="uname"></param>
        /// <param name="upwd"></param>
        /// <param name="utel"></param>
        /// <param name="utype"></param>
        /// <returns></returns>
        public int add(string uname,string upwd,string utel,int utype)
        {

            byte[] result = Encoding.Default.GetBytes(upwd.Trim());
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);
            string pwd = BitConverter.ToString(output);  //Repplace 替换
             userinfor user = new userinfor();
             user.username = uname;
             user.userpwd = pwd;
             user.userphone = utel;
             user.usertype = utype;
             if (service.Add(user) > 0)
             {
                 return 1;
             }
             else
             {
                 return 0;
             }
          
        }
        /// <summary>
        /// 判断用户名存不存在
        /// </summary>
        /// <param name="uname"></param>
        /// <returns></returns>
        public int  getuname(string uname)
        {
           DataSet ds = service.finduName(uname);
           if(ds.Tables[0].Rows.Count>0)
           {
               return 1;

           }
           else
            {
                return 0;
            }
        }
    }
}
