﻿function MyAlert(Title, Content, BtnText, issuccess) {
    $("#myModal").remove();
    var MyAlertHTML = new Array();
    MyAlertHTML.push("<div class=modal fade' id='myModal'>");
    MyAlertHTML.push("<div class='modal-dialog'>");
    MyAlertHTML.push("<div class='modal-content'>");
    MyAlertHTML.push(" <div class='modal-header'>");
    MyAlertHTML.push("<button class='close' data-dismiss='modal'>");
    MyAlertHTML.push("&times;");
    MyAlertHTML.push("</button> ");
    MyAlertHTML.push("<h4>");
    MyAlertHTML.push(Title);
    MyAlertHTML.push("</h4>");
    MyAlertHTML.push(" </div>");
    MyAlertHTML.push(" <div class='modal-body'>");
    MyAlertHTML.push(Content);
    MyAlertHTML.push("</div>");
    MyAlertHTML.push("<div class='modal-footer'>");
    MyAlertHTML.push("<button data-dismiss='modal'>");
    MyAlertHTML.push(BtnText);
    MyAlertHTML.push("</button>");
    MyAlertHTML.push("</div>");
    MyAlertHTML.push("</div>");
    MyAlertHTML.push("</div>");
    MyAlertHTML.push("</div>");
    $("body").append(MyAlertHTML.join(""));
    $("#myModal").modal("show");
    if (issuccess) {
        $("#myModal").on('hidden.bs.modal', function () {
            location.href = "/Home/Index";
        })
    }
}