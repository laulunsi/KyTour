(function ($) {
    Date.prototype.format = function (format) {
        var o = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3),
            "S": this.getMilliseconds()
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
            (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
                RegExp.$1.length == 1 ? o[k] :
                    ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    };
    $.extend({
        tripcalendar: function () {
            var date1 = arguments[0];
            var date2 = arguments[1];
            var data_type = arguments[2];
            var count = 2;           
            if (!date1)
                return;
            if (!date2)
                count = 1;
            var currdate = new Date();
            if (date1)
                $(date1).val(currdate.format('yyyy-MM-dd'));
            currdate.setDate(currdate.getDate() + 1);
            if (date2)
                $(date2).val(currdate.format('yyyy-MM-dd'));
            var _minDate = new Date;
            var _maxDate = new Date;
            var _currentDate = new Date;
            _maxDate.setFullYear(_currentDate.getFullYear() + 1);
            if (arguments[2] == false) {
                //_maxDate.setFullYear(_currentDate.getFullYear() - 2);
                _minDate = null;
            } else {
                //_maxDate = null;
                //_minDate = null;
            }
            var _isSelect = false;
            if (count == 1)
                _isSelect = true;
            YUI({
                modules: {
                    'trip-calendar': {
                        fullpath: '/js/public/trip-calendar.js',
                        type: 'js',
                        requires: ['trip-calendar-css']
                    },
                    'trip-calendar-css': {
                        fullpath: '/js/public/trip-calendar.css',
                        type: 'css'
                    }
                }               
            }).use('trip-calendar', function (Y) {
                var oCal = new Y.TripCalendar({
                    minDate: _minDate,//最小时间限制
                    maxDate: _maxDate,//最大时间限制
                    triggerNode: date1, //第一个触节点
                    finalTriggerNode: date2, //最后一个触发节点
                    count: count,
                    isSelect: _isSelect
                });


                oCal.on('dateclick', function () {
                    var selectedDate = this.get('selectedDate');
                    var rDate = /^((19|2[01])\d{2})-(0?[1-9]|1[012])-(0?[1-9]|[12]\d|3[01])$/;
                    oDepDate = Y.one(date1);
                    sDepDate = oDepDate.get('value');
                    oEndDate = Y.one(date2);
                    if (oEndDate)
                        sEndDate = oEndDate.get('value');
                    aMessage = ['请选择出发日期', '请选择返程日期', '返程时间不能早于出发时间，请重新选择', '日期格式错误'];
                    iError = -1;


                    if ((oEndDate && (sDepDate.replace(/-/g, '') >= sEndDate.replace(/-/g, '')) || (datecalculate(sEndDate, sDepDate) > 28)) && data_type == "h") {

                        var _tempdate = this._toDate(sDepDate);
                        _tempdate.setDate(_tempdate.getDate() + 1);
                        $(date2).val(this._toStringDate(_tempdate));

                        if (datecalculate(sEndDate, sDepDate) > 28)
                            $("#txtEndDate").attr('title', '您入住酒店时间超过28天，请分订单提交预订').css('border', '1px solid #D80000');
                        else
                            $("#txtEndDate").attr('title', '您入住酒店时间小于1天').css('border', '1px solid #D80000');
                    }
                    else if (data_type == "h") {

                        $("#txtEndDate").attr('title', '').css('border', '1px solid #d2d2d2');
                        $("#txtEndDate").attr('title', '').css('border', '1px solid #d2d2d2');
                    }
                    else if (oEndDate && (sDepDate.replace(/-/g, '') > sEndDate.replace(/-/g, ''))) {
                        var _tempdate = this._toDate(sDepDate);
                        _tempdate.setDate(_tempdate.getDate() + 1);
                        $(date2).val(this._toStringDate(_tempdate));
                    }
                });
                oCal.on("endDateChange", function (e) {
                    var day = oCal._getDateDiff($(date2).val(), $(date1).val());
                    $("#txtnumday").val(day);
                })
            });
        },
        datecalculate: function (enddate, startdate) {
            var s1 = new Date(startdate.replace(/-/g, "/"));
            var s2 = new Date(enddate.replace(/-/g, "/"));
            var days = s2.getTime() - s1.getTime();
            var mDays = parseInt(days / (1000 * 60 * 60 * 24));
            return mDays;
        }
    });
})(jQuery);

