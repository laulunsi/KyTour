
var cbdtype;
var keywordid;
var keywords;
var ltype = 1;
var iscollction = 0;//是否收藏 0未收藏 1已收藏
var userid = 0;
var keywordhtml = "";
var reqparam = {
    hotelid: "",
    checkindate: "",
    checkoutdate: "",
    userid: userid
};
$(function () {

    HotelDetail.Init();
    HotelDetail.BindCity();
    HotelDetail.BindKeyWord();
    $("#txtkeywords").click(function () {
        if ($(this).val() == "" || $(this).val() == null) {
            if (keywordhtml != null && keywordhtml != "") {
                $("#Keyword_box_1").show();
            }
            $("#suggest_city_txtkeywords").hide();
            $("#pop_city_txtkeywords").hide();
        }
        else {
            $("#Keyword_box_1").hide();
            $("#suggest_city_txtkeywords").show();
            $("#pop_city_txtkeywords").show();

        }
    })

    //查询房型
    $("#btnsearchroom").click(function () {
        HotelDetail.GetRoomInfo($("#tstartdate").val(), $("#tenddate").val());
    })
    document.getElementById("txtkeywords").oninput = function () {
        if ($(this).val() == "" || $(this).val() == null) {
            if (keywordhtml != null && keywordhtml != "") {
                $("#Keyword_box_1").show();
            }
            $("#suggest_city_txtkeywords").hide();
            $("#pop_city_txtkeywords").hide();
        }
        else {
            $("#Keyword_box_1").hide();
            $("#suggest_city_txtkeywords").show();
            $("#pop_city_txtkeywords").show();
            HotelDetail.BindHotelKeyword();
        }
    }

    $("#closeBox").click(function () {
        $(this).parent().hide();
    })
    $("body").click(function () {
        var myInput = document.getElementById('txtkeywords');
        if (myInput == document.activeElement) {//当前文本框获得焦点
        }
        else {
            //未获得焦点
            $("#Keyword_box_1").hide();
            $("#suggest_city_txtkeywords").hide();
            $("#pop_city_txtkeywords").hide();
        }
    })
    $("#sqlist").on("click", "dd", function () {
        keywordid = $(this).attr("id");
        cbdtype = $(this).attr("data");
        $("#txtkeywords").val($(this).find("a").html());
        HotelDetail.SubmitData();
    })

    $("#btnsearch").click(function () {
        HotelDetail.SubmitData();
    })

    HotelDetail.InitGroupPhotos();
});


var HotelDetail = {
    //组图
    InitGroupPhotos: function () {
        //初始化第一张展示图
        $('#bigshowimg').attr('src', $('#GroupPhotos .sp_Smallphotos').eq(0).find('img').eq(0).attr('mainimgsrc')).attr('parent_index', 0).attr('self_index', 0);
        $("#bigshowimg").attr("onerror", "this.src='/Images/hotelnoimg.png'");
        var bigimgsrc = $("#bigshowimg").attr("src");
        if (bigimgsrc == null || bigimgsrc == "") {
            $("#bigshowimg").attr("src", "/Images/hotelnoimg.png");
        }
        $("#bigshowimg").attr('littleimgsrc', $('#GroupPhotos .sp_Smallphotos').eq(0).find('img').eq(0).attr('mainimgsrc')).attr('parent_index', 0).attr('self_index', 0);
        $('#hotelPhotos_category a').click(function () {
            var _index = parseInt($(this).attr('data'));
            $('#GroupPhotos .sp_Smallphotos').eq(_index).addClass('active').siblings().removeClass('active');
            var $targetImg = $('#GroupPhotos .sp_Smallphotos').eq(_index).find('img').eq(0);
            //初始化第一张图
            $('#bigbannerimg').attr('src', $targetImg.attr('bigimgsrc')).attr('parent_index', _index).attr('self_index', 0);
            $('.sp_category a').eq(_index).addClass('active').siblings().removeClass('active');
            $('body').append('<div class="ui-widget-overlay000"></div>');
            $('#silderPhotos').show();
        });

        $('#bigshowimg').click(function () {
            $('#GroupPhotos .sp_Smallphotos').eq(0).addClass('active').siblings().removeClass('active');
            var $targetImg = $('#GroupPhotos .sp_Smallphotos').eq(0).find('img').eq(0);
            $('#bigbannerimg').attr('src', $targetImg.attr('bigimgsrc')).attr('parent_index', 0).attr('self_index', 0);
            $('.sp_category a').eq(0).addClass('active').siblings().removeClass('active');
            $('body').append('<div class="ui-widget-overlay000"></div>');
            $('#silderPhotos').show();
        })
        //切换组图
        $('.sp_category a').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            $('#GroupPhotos .sp_Smallphotos').eq($('.sp_category a').index($(this))).addClass('active').siblings().removeClass('active');
        });
        //选中图片
        $('#GroupPhotos').on("click", "li", function () {
            $(this).addClass('active').siblings().removeClass('active');
            var $thisimg = $(this).find('img');
            var parent_index = $('#GroupPhotos .sp_Smallphotos').index($thisimg.parents('.sp_Smallphotos'));
            var self_index = $thisimg.parents('ul').find('li').index($thisimg.parents('li'));
            $('#bigbannerimg').attr('src', $thisimg.attr('bigimgsrc')).attr('parent_index', parent_index).attr('self_index', self_index);
        });
        //上一张
        $('#banner_prev').click(function () {
            banner_switch(this, 'prev');
        });
        //下一张
        $('#banner_next').click(function () {
            banner_switch(this, 'next');
        });
        $('#banner_close').click(function () {
            $('.ui-widget-overlay000').remove();
            $('#silderPhotos').hide();
        });
        function banner_switch(imgobj, type) {
            var $banner = $(imgobj).parents('.sp_img').find('#bigbannerimg');
            var parent_index = $banner.attr('parent_index');
            var self_index = $banner.attr('self_index');
            var curr_index = 0;
            if (type == 'prev') {
                if (parseInt(self_index) == 0)
                    return;
                curr_index = parseInt(self_index) - 1
            }
            else {
                var max = $('#GroupPhotos .sp_Smallphotos').eq(parent_index).find('img').length;
                if (parseInt(self_index) >= max - 1)
                    return;
                curr_index = parseInt(self_index) + 1
            }
            var $targetImg = $('#GroupPhotos .sp_Smallphotos').eq(parent_index).find('img').eq(curr_index);
            $('#bigbannerimg').attr('src', $targetImg.attr('bigimgsrc')).attr('parent_index', parent_index).attr('self_index', curr_index);
        }

    },
    Init: function () {
        $.tripcalendar('#txtStartDate', '#txtEndDate', 'h');
        $.tripcalendar('#tstartdate', '#tenddate', 'h');
        var cId = getQueryString("cityid") == null || getQueryString("cityid").length == 0 ? HotelCityList[1][0] : getQueryString("cityid");
        var cName = HotelCityList[cId][1];

        var currdate = new Date();
        var sDate = getQueryString("sdate") == null || getQueryString("sdate").length == 0 ? formatterDate(currdate) : getQueryString("sdate");

        currdate.setDate(currdate.getDate() + 1);
        var eDate = getQueryString("edate") == null || getQueryString("edate").length == 0 ? formatterDate(currdate) : getQueryString("edate");


        $("#txtCityId").val(cId);
        $("#txtcityname").val(cName);

        $("#txtStartDate").val(sDate);
        $("#txtEndDate").val(eDate);
        $("#tstartdate").val(sDate);
        $("#tenddate").val(eDate);
        var hotelid = getQueryString("hotelid");
        reqparam.hotelid = hotelid;
        reqparam.checkindate = $("#txtStartDate").val();
        reqparam.checkoutdate = $("#txtEndDate").val();
        HotelDetail.JudgeIsLogin();//是否登录

        HotelDetail.GetCollection();//是否收藏
        HotelDetail.CollectionOperate();
        HotelDetail.GetRoomInfo();//查询房间列表
        _Map.init_Map();
        _Map.init_bigMap();
        HotelDetail.ScrollImg();
        $("#span_innerscore").parent().addClass("addinnerscore");
    },
    //绑定城市事件
    BindCity: function () {
        $('#txtcityname').querycity({
            'data': HotelCityList, 'tabs': HotelTabCityList, 'hotList': HotelHotCityList, 'ispopcharsort': '1', 'nextfocus': 'txtcityname', 'getvaluefun':
                function () {
                    HotelDetail.BindKeyWord();
                }, 'txtindex': 'txtCityId^1'
        });
    },
    //绑定关键字
    BindKeyWord: function () {
        var queryData = {
            cityid: $("#txtCityId").val(),
            iscbd: 1,
            isareainfo: 1,
            islocation: 1,
            ismetrostat: 1,
            isfacility: 0,
            ishotelband: 0
        }
        var common = "hotel_gethotelcbd";
        post(common, queryData, bindData);
        function bindData(result) {
            if (result.status == 1) {
                var html = "";
                var sq = result.data.hotelcbd;
                var xzq = result.data.hotelare;
                var jchcz = result.data.hotelloion;
                var dtx = result.data.hotelmetrostat;

                if (sq != null && sq.length > 0) {
                    var sqhtml = "<dt>热门商圈</dt>";
                    $.each(sq, function (i, item) {
                        sqhtml += "<dd id='" + item.id + "' data='" + item.ltype + "'><a href='javascript:void(0)'>" + item.cbdname + "</a></dd>";
                    })
                    html += sqhtml;
                }
                if (xzq != null && xzq.length > 0) {
                    var xzqhtml = "<dt>行政区</dt>";
                    $.each(xzq, function (k, item) {
                        xzqhtml += "<dd id='" + item.id + "' data='" + item.ltype + "'><a href='javascript:void(0)'>" + item.cbdname + "</a></dd>";
                    })
                    html += xzqhtml;
                }
                if (jchcz != null && jchcz.length > 0) {
                    var jchczhtml = "<dt>机场火车站</dt>";
                    $.each(jchcz, function (j, item) {
                        jchczhtml += "<dd id='" + item.id + "' data='" + item.ltype + "'><a href='javascript:void(0)'>" + item.cbdname + "</a></dd>";
                    })
                    html += jchczhtml;
                }
                if (dtx != null && dtx.length > 0) {
                    var dtxhtml = "<dt>地铁线</dt>";
                    $.each(dtx, function (j, item) {
                        dtxhtml += "<dd id='" + item.id + "' data='" + item.ltype + "'><a href='javascript:void(0)'>" + item.cbdname + "</a></dd>";
                    })
                    html += dtxhtml;
                }
                keywordhtml = html;
                $("#sqlist").html(html);
            }
        }


    },
    BindHotelKeyword: function () {
        var query = {
            requestway: 2,
            pageindex: 1,
            pagesize: 10,
            cityid: $("#txtCityId").val(),
            keyword: $.trim($("#txtkeywords").val())
        };
        var proid = HotelCityList[$("#txtCityId").val()][4];
        post("hotel_hotelkeyword", query, bindAutoData);
        function bindAutoData(result) {
            if (result.status == 1) {
                var list = [];
                $.each(result.data.hotelkeywordlist, function (index, item) {
                    list[index] = new Array(item.hotelid.toString(), item.keyword, item.typename, item.Keywordtype.toString(), item.ltype.toString(), item.subway.toString(), item.nameshort, item.en);
                });
                $('#txtkeywords').querykeyword({
                    'data': list, 'hotList': [], 'suggestTitleText': '输入关键字/地标/商圈或↑↓选择', 'isshowpop': '0', 'txtindex': 'keywordtype^4~keywordid^1~ltype^5~subway^6', 'getvaluefun': function () {
                        keywordid = $("#keywordid").val();
                        cbdtype = $("#ltype").val();
                        switch ($('#ltype').val()) {
                            case "1"://酒店
                                window.location.href = '/Hotel/Hotel/' + proid + '/' + $('#txtCityId').val() + '/' + $('#keywordid').val() + '/index.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&hotelid=' + $('#keywordid').val() + "&k=" + escape($("#txtkeywords").val());
                                break;
                            case "2": //城市
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val();
                                break;
                            case "3"://商圈
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&sl=' + $('#keywordid').val() + "&ltype=" + $("#ltype").val() + "&k=" + escape($("#txtkeywords").val());
                                break;
                            case "4"://行政区
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&sl=' + $('#keywordid').val() + "&ltype=" + $("#ltype").val() + "&k=" + escape($("#txtkeywords").val());
                                break;
                            case "5"://机场火车站
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + "&sl=" + $('#keywordid').val() + "&ltype=" + $("#ltype").val() + "&k=" + escape($("#txtkeywords").val());
                                break;
                            case "6"://地铁线
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&subwayid=' + $('#keywordid').val() + "&sl=0" + "&ltype=" + $("#ltype").val() + "&k=" + escape($("#txtkeywords").val());
                                break;
                            case "7"://景点
                                break;
                            case "8"://地标
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&subwayid=' + "&sl=" + $("#keywordid").val() + "&k=" + escape($("#txtkeywords").val()) + "&ltype=" + $("#ltype").val();
                                break;
                            case "9"://地铁站
                                window.location.href = '/Hotel/HotelList.shtml?cityid=' + $('#txtCityId').val() + '&sdate=' + $("#txtStartDate").val() + '&edate=' + $("#txtEndDate").val() + '&subwayid=' + $("#subway").val() + "&sl=" + $('#keywordid').val() + "&ltype=" + $("#ltype").val() + "k=" + escape($("#txtkeywords").val());
                                break;
                            default:
                                break;
                        }
                    }
                });
            }
        }
    },
    //调接口查询酒店详情
    GetHotelDetail: function () {
        var common = "hotel_gethoteldetail";
        post(common, reqparam, bindata);
        function bindata(result) {
            if (result.status == 1) {
                //var facilitylist = result.data.facilitylist;//设施列表
                //var imglist = result.data.imglist;//图片列表
                //var environmentlist = result.data.ennvironmentlist;//周边环境

                //iscollction = result.data.iscollect;//是否收藏
                //HotelDetail.BindHotelInfo(result.data, imglist, environmentlist, facilitylist);

            }
        }
    },
    //查询收藏
    GetCollection: function () {

        var _myuser = _G.GetCookie("_user");
        if (_myuser != null) {
            userid = JSON.parse(_myuser).userid;
        }
        var req = {
            userid: userid,
            hotelid: reqparam.hotelid
        }
        var common = "hotel_getcollectbyuser";
        post(common, req, bindcollection);
        function bindcollection(result) {
            if (result.status == 1) {

                if (result.data != null && result.data != "") {
                    iscollction = 1;
                }
                else {
                    iscollction = 0;
                }
            }


            // HotelDetail.CollectionOperate();
            HotelDetail.ToggleCollection();
        }


    },
    SubmitData: function () {
        if (cbdtype == null || cbdtype == undefined) {
            cbdtype = "";
        }
        if (keywordid == null || keywordid == undefined) {
            keywordid = "";
        }
        location.href = "/Hotel/HotelList.shtml?cityid=" + $("#txtCityId").val() + "&sdate=" + $("#txtStartDate").val() + "&edate=" + $("#txtEndDate").val() + "&ltype=" + cbdtype + "&sl=" + keywordid + "&k=" + escape($("#txtkeywords").val());
    },
    GetRoomInfo: function (date1, date2) {
        /********查询房间列表********/
        $("#divroomlist").html(HotelDetail.loading);
        var requestdata = {
            hotelid: reqparam.hotelid,
            checkindate: $("#txtStartDate").val(),
            checkoutdate: $("#txtEndDate").val(),
            userid: userid,
            cid: 0
        }
        if (date1 != null && date1 != undefined && date2 != null && date2 != undefined) {
            requestdata.checkindate = date1;
            requestdata.checkoutdate = date2;
        }
        var common = "hotel_getroomlist";
        post(common, requestdata, bindroom);
        function bindroom(result) {
            if (result.status == 1) {
                var datalist = result.data;
                HotelDetail.BindRoomInfo(datalist);

            }
            else {
                $("#divroomlist").html(result.message);
            }
        }
    },
    //绑定酒店基本信息及图片
    BindHotelInfo: function (data, imglist, environmentlist, facilitylist) {
        $("#bigshowimg").attr("src", imglist[0].b_img);//大图片
        var totalcount = imglist.length;
        var hobbyimgcount = 0;//大厅图片数量
        var roomimgcount = 0;//房间图片数量
        var hobbyimglist = [];
        var roomimglist = [];
        $.each(imglist, function (i, item) {
            if (item.pictype == 2) {
                hobbyimgcount++;
                hobbyimglist.push(item);
            }
            if (item.pictype == 4) {
                roomimgcount++;
                roomimglist.push(item);
            }
        })


        /*****************酒店基本信息********************/
        $("#h_hotelname").html(data.hotelname);//酒店名称
        $("#span_address").html(data.address);//地址
        $("#span_address").attr("x", data.lng);
        $("#span_address").attr("y", data.lat);
        var cbdname = (data.cbdname != null && data.cbdname != "") ? "【" + data.cbdname + "】" : data.cbdname;
        $("#span_cbdname").html(cbdname);//商圈
        $("#span_innerscore").html(data.innerscore);//内部评分
        $("#span_lowprice").html();//价格

        _Map.init_Map();
        _Map.init_bigMap();
        //图片数量
        $("#allimgcount").html("全部图片(" + totalcount + ")");
        $("#hobbyimgcount").html("大厅图片(" + hobbyimgcount + ")");
        $("#roomimgcount").html("房间图片(" + roomimgcount + ")");
        $("#all_imgcount").html("全部图片(" + totalcount + ")");
        $("#hobby_imgcount").html("大厅图片(" + hobbyimgcount + ")");
        $("#room_imgcount").html("房间图片(" + roomimgcount + ")");

        /***********所有图片开始****************/

        var allimghtml = "";
        $.each(imglist, function (j, allitem) {
            if (j == 0) {
                allimghtml += "<li><img src='" + allitem.url + "' bigimgsrc='" + allitem.b_img + "' mainimgsrc='" + allitem.b_img + "' alt='' /></li>";
            }
            else {
                allimghtml += "<li><img src='" + allitem.url + "' bigimgsrc='" + allitem.b_img + "' alt='' /></li>";
            }
        })
        $("#ulallimg").html(allimghtml);


        /*************大厅图片*************************/
        var hobbyimghtml = "";
        $.each(hobbyimglist, function (k, hitem) {
            hobbyimghtml += "<li><img src='" + hitem.url + "' bigimgsrc='" + hitem.b_img + "' alt='' /></li>";
        })
        $("#ulhobbyimg").html(hobbyimghtml);

        /************房间图片********************/
        var roomimghtml = "";
        $.each(roomimglist, function (b, roomitem) {
            roomimghtml += "<li><img src='" + roomitem.url + "' bigimgsrc='" + roomitem.b_img + "' alt='' /></li>";
        })
        $("#ulroomimg").html(roomimghtml);

        /************设施**************/
        var fhtml = "";
        var facilityname = "";
        var spfacilityhtml = "";
        $.each(facilitylist, function (f, fitem) {
            if (f > 0) {
                facilityname += "、";
            }
            facilityname += fitem.facilityname;
            fhtml += "<i class='icons-facility" + fitem.id + "' title='" + fitem.facilityname + "'></i>";

            spfacilityhtml += "<li class='facility-" + fitem.id + "'><icon></icon><span>" + fitem.facilityname + "</span><i></i></li>";

        })
        $(".hr_info_server").html(fhtml);
        $("#span_sp_facility").html(spfacilityhtml);

        //酒店介绍
        var introduce = data.introduce;
        $("#div_hotelintroduce").html(introduce);
        //酒店信息
        var detail = data.hoteldetails;
        var hotelinfohtml = "<div class=\"hlcb_base\">";
        hotelinfohtml += "<span class=\"title\">基本信息：</span>";
        hotelinfohtml += "<p>" + detail + "</p>"
        hotelinfohtml += "</div>";
        hotelinfohtml += "<div class=\"hlcb_base\">";
        hotelinfohtml += "<span class=\"title\">酒店设施：</span>";
        hotelinfohtml += "<p>" + facilityname + "</p>";
        hotelinfohtml += "</div>";
        $("#div_hotelbaseinfo").html(hotelinfohtml);
        //周边环境
        var environmenthtml = "";
        var jingdianhtml = "<span class=\"sp_title\">酒店周边</span>";
        $.each(environmentlist, function (e, eitem) {
            environmenthtml += "<div class=\"hlcb_base\"><span class=\"title\">" + eitem.falilityname + "：</span><p>" + eitem.facilitycontent + "</p> </div>";
            if (eitem.falilityname == "景点" || eitem.falilityname == "地铁") {
                var jingdian = eitem.facilitycontent;
                var spotlist = jingdian.split('、');
                for (var dd = 0; dd < spotlist.length; dd++) {

                    jingdianhtml += "<span class=\"sper_details\">" + spotlist[dd] + "</span>";

                }
            }
        })
        $("#div_environment").html(environmenthtml);
        $("#span_sp_environment").html(jingdianhtml);


        HotelDetail.ScrollImg();//图片滑动
    },
    //图片的滑动
    ScrollImg: function () {
        var allimgcurrent = 1;
        var hobbyimgcurrent = 1;
        var roomimgcurrent = 1;
        var allimg_i = 1;
        var hobbyimg_i = 1;
        var roomimg_i = 1;
        var dir_all = true;
        var dir_hobby = true;
        var dir_room = true;
        var allimg_pagesize = Math.ceil($("#ulallimg").find("li").length / 5);
        var hobbyimg_pagesize = Math.ceil($("#ulhobbyimg").find("li").length / 5);
        var roomimg_pagesize = Math.ceil($("#ulroomimg").find("li").length / 5);
        function goNext(which) {
            switch (which) {
                case "all":
                    if (allimgcurrent == allimg_pagesize) {
                        dir_all = false;
                        return;
                    }
                    seekTo(allimgcurrent * 5, which);
                    allimgcurrent++;
                    break;
                case "hobby":
                    if (hobbyimgcurrent == hobbyimg_pagesize) {
                        dir_hobby = false;
                        return;
                    }
                    seekTo(hobbyimgcurrent * 5, which);
                    hobbyimgcurrent++;
                    break;
                case "room":
                    if (roomimgcurrent == roomimg_pagesize) {
                        dir_room = false;
                        return;
                    }
                    seekTo(roomimgcurrent * 5, which);
                    roomimgcurrent++;
                    break;
                default:
                    break;
            }

        }
        function goPrev(which) {
            if (which == "all") {
                if (allimgcurrent == 1) {
                    dir_all = true;
                    return;
                }
                allimgcurrent--;
                seekTo(allimgcurrent * (allimgcurrent - 1), which);
            }
            if (which == "hobby") {
                if (hobbyimgcurrent == 1) {
                    dir_all = true;
                    return;
                }
                hobbyimgcurrent--;
                seekTo(hobbyimgcurrent * (hobbyimgcurrent - 1), which);
            }
            if (which == "room") {
                if (roomimgcurrent == 1) {
                    dir_room = true;
                    return;
                }
                roomimgcurrent--;
                seekTo(roomimgcurrent * (roomimgcurrent - 1), which);
            }

        }

        function seekTo(i, which) {
            if (which == "all") {
                var left = $("#ulallimg").find("li").eq(i).position().left;
                $("#ulallimg").animate({ left: -left }, 300, 'swing');
            }
            if (which == "hobby") {
                var left = $("#ulhobbyimg").find("li").eq(i).position().left;
                $("#ulhobbyimg").animate({ left: -left }, 300, 'swing');
            }
            if (which == "room") {
                var left = $("#ulroomimg").find("li").eq(i).position().left;
                $("#ulroomimg").animate({ left: -left }, 300, 'swing');
            }

        }
        $("#a_allimg_prev").click(function () {
            goPrev("all");
        })
        $("#a_allimg_next").click(function () {
            goNext("all");
        })
        $("#a_hobbyimg_prev").click(function () {
            goPrev("hobby");
        })
        $("#a_hobbyimg_next").click(function () {
            goNext("hobby");
        })
        $("#a_roomimg_prev").click(function () {
            goPrev("room");
        })
        $("#a_roomimg_next").click(function () {
            goNext("room");
        })
    },
    //绑定房型列表
    BindRoomInfo: function (roomlist) {
        var divroomlisthtml = "";
        var lowprice = 0;
        var tehuiroom = {
            roomid: 0,
            roomtypeid: 0,
            bedstr: "",
            roomname: "",
            typename: "",
            lowprice: 0
        }
        $.each(roomlist, function (i, item) {
            divroomlisthtml += "<div class=\"hl_body-box clearfix\">";
            divroomlisthtml += "<div class=\"fl hl_imgbox\">";


            if (item.url == "" || item.url == null) {
                item.url = "/Images/noroomimg.png";
            }
            divroomlisthtml += "<img  onerror=\"this.src='/Images/noroomimg.png'\" src='" + item.url + "' alt='' />";
            divroomlisthtml += " <div class=\"hl_imgbox_w\">";
            divroomlisthtml += "<span class='title'>" + item.roomname + "</span>";
            divroomlisthtml += "<a href=\"javascript:void(0)\" class=\"lookdetail\">查看详情";
            divroomlisthtml += "<div class=\"showdetail\">";
            divroomlisthtml += "<ul>";
            divroomlisthtml += "<li title=''>建筑面积：" + item.roomarea + "</li>";
            divroomlisthtml += "<li title=''>楼层：" + item.floor + "</li>";
            divroomlisthtml += "<li title=''>床型：" + item.bedsize + "</li>";
            divroomlisthtml += "<li title=''>无烟房：" + item.isnosmokingroom + "</li>";
            divroomlisthtml += "<li title=''>宽带：" + item.broadband + "</li>";
            divroomlisthtml += "<li title=''>最多入住人数：" + item.personsstr + "</li>";
            divroomlisthtml += "</ul>";
            divroomlisthtml += "</div>";
            divroomlisthtml += "</a>";
            divroomlisthtml += "</div>";
            divroomlisthtml += "</div>";
            divroomlisthtml += "<div class=\"fr hl_houselist\">";

            var saleroomtypelist = item.saleroomtypelist;
            $.each(saleroomtypelist, function (j, ritem) {
                var iscanbook = true;
                var roompricelist = ritem.roompricelist;
                var chkcount = 0;
                $.each(roompricelist, function (oneprice, pitem) {
                    if (pitem.stock == 0 || pitem.realityprice == 0) {
                        chkcount++;
                    }
                })
                if (chkcount > 0) {
                    iscanbook = false;
                }
                if (lowprice == 0) {
                    lowprice = ritem.realityprice;
                    tehuiroom.roomid = item.roomid;
                    tehuiroom.roomtypeid = ritem.roomtypeid;
                    tehuiroom.roomname = item.roomname;
                    tehuiroom.bedstr = ritem.bedstr;
                    tehuiroom.typename = ritem.typename;
                    tehuiroom.lowprice = parseFloat(ritem.realityprice).toFixed(2);
                }
                else {
                    if (parseFloat(ritem.realityprice) < parseFloat(lowprice)) {
                        lowprice = ritem.realityprice;
                        tehuiroom.roomid = item.roomid;
                        tehuiroom.roomtypeid = ritem.roomtypeid;
                        tehuiroom.roomname = item.roomname;
                        tehuiroom.bedstr = ritem.bedstr;
                        tehuiroom.typename = ritem.typename;
                        tehuiroom.lowprice = parseFloat(ritem.realityprice).toFixed(2);
                    }
                }
                divroomlisthtml += "<ul>";
                divroomlisthtml += "<li>" + ritem.bedstr + "</li>";
                divroomlisthtml += "<li>" + ritem.typename + "</li>";
                divroomlisthtml += "<li>" + item.broadband + "</li>";
                divroomlisthtml += "<li>￥" + parseFloat(ritem.realityprice).toFixed(0) + "</li>";
                divroomlisthtml += "<li>" + (ritem.paytype == 1 ? '<span class="icon_prepay" title="需预先支付房款">预付</span>' : '') + (iscanbook == true ? "<a href='javascript:HotelDetail.GoToBook(" + item.roomid + ',' + ritem.roomtypeid + ")' class=\"btn_reserve\">预订</a>" : "<span class=\"txt_reserve\">订完</span>") + "</li>";
                divroomlisthtml += "</ul>";
            })

            divroomlisthtml += "</div>";
            divroomlisthtml += "</div>";
        })

        $("#divroomlist").html(divroomlisthtml);
        $("#span_lowprice").html(parseFloat(lowprice).toFixed(0));
        HotelDetail.BindTejiaRoom(tehuiroom);
    },
    //绑定特价房型
    BindTejiaRoom: function (tehuiroom) {
        var tehuiroomhtml = "";
        tehuiroomhtml += "<div class=\"sp_Infrastructure\">";
        tehuiroomhtml += "<span class=\"sp_title\">特价房型</span>";
        tehuiroomhtml += "<span class=\"sp_details\">房型：" + tehuiroom.roomname + "</span>";
        tehuiroomhtml += "<span class=\"sp_details\">床型：" + tehuiroom.bedstr + "</span>";
        tehuiroomhtml += "<span class=\"sp_details\">早餐：" + tehuiroom.typename + "</span>";
        //tehuiroomhtml += "</div>";
        //tehuiroomhtml += "<div class=\"sp_submit_box\">";
        tehuiroomhtml += "<span class=\"sp_Symbol\">￥<span class=\"sp_price\">" + tehuiroom.lowprice + "</span></span>";
        tehuiroomhtml += "<input type=\"button\" value=\"立即预订\" class=\"sp_reserve\" onclick='javascript:HotelDetail.GoToBook(" + tehuiroom.roomid + "," + tehuiroom.roomtypeid + ")' />";
        tehuiroomhtml += "</div>";
        $("#div_tejiaroom").html(tehuiroomhtml);
    },
    //跳转到填写订单页
    GoToBook: function (roomid, roomtypeid) {
        $('.ui-widget-overlay000').remove();
        $('#silderPhotos').hide();
        var url = "/Hotel/HotelFillOrder.shtml?hotelid=" + reqparam.hotelid + "&roomid=" + roomid + "&roomtypeid=" + roomtypeid + "&sDate=" + $("#tstartdate").val() + "&eDate=" + $("#tenddate").val() + "&num=1";
        CheckUserLogin(url, "target", 3);
        //  location.href = "/Hotel/HotelFillOrder.shtml?hotelid=" + reqparam.hotelid + "&roomid=" + roomid + "&roomtypeid=" + roomtypeid + "&sDate=" + $("#txtStartDate").val() + "&eDate=" + $("#txtEndDate").val() + "&num=1";
    },
    //添加收藏
    AddCollection: function () {
        // var url = "HotelDetail.shtml?cityid=" + $("#txtCityId").val() + "&hotelid=" + reqparam.hotelid + "&sdate=" + reqparam.checkindate + "&edate=" + reqparam.checkoutdate;
        // CheckUserLogin(url, "", 2, add_collection);
        //function add_collection()
        //{
        var _myuser = _G.GetCookie("_user");
        if (_myuser != null) {
            userid = JSON.parse(_myuser).userid;
        }
        var req = {
            hotelid: reqparam.hotelid,
            userid: userid,
        }

        var common = "hotel_addhotelcollect";
        post(common, req, showresult);
        function showresult(result) {
            if (result.status == 1) {
                iscollction = 1;//收藏成功
                // alert("收藏成功");
                HotelDetail.ToggleCollection();

            } else {
                alert(result.message);
            }
        }
        // }


    },
    //取消收藏
    DelCollection: function () {
        //var url = "HotelDetail.shtml?cityid=" + $("#txtCityId").val() + "&hotelid=" + reqparam.hotelid + "&sdate=" + reqparam.checkindate + "&edate=" + reqparam.checkoutdate;
        //CheckUserLogin(url, "", 2, del_collection);
        //function del_collection()
        //{
        var _myuser = _G.GetCookie("_user");
        if (_myuser != null) {
            userid = JSON.parse(_myuser).userid;
        }
        var req1 = {
            hotelid: reqparam.hotelid,
            userid: userid,
        }
        var common = "hotel_delhotelcollect";
        post(common, req1, showresult);
        function showresult(result) {
            if (result.status == 1) {
                iscollction = 0;//取消收藏
                // alert("取消成功");
                HotelDetail.ToggleCollection();
            }
            else {
                alert(result.message);
            }
        }
        //}


    },
    CollectionOperate: function () {
        $("#a_collecthotel").click(function () {
            if (userid == 0) {
                var url = window.location.href;
                CheckUserLogin(url, "", 2, null);

                function callback_fun() {
                    if (iscollction == 1) {
                        HotelDetail.DelCollection();
                    }
                    if (iscollction == 0) {
                        HotelDetail.AddCollection();
                    }
                }
            }
            else {
                if (iscollction == 1) {
                    HotelDetail.DelCollection();
                }
                if (iscollction == 0) {
                    HotelDetail.AddCollection();
                }
            }

        })
    },
    //收藏的操作
    ToggleCollection: function () {
        if (iscollction == 1) {
            $("#a_collecthotel").attr("title", "取消收藏");
            $("#a_collecthotel").html("取消收藏");
            $("#a_collecthotel").parents(".hr_info_right").removeClass("colle1").addClass("colle2");
        }
        else {
            $("#a_collecthotel").attr("title", "添加收藏");
            $("#a_collecthotel").html("收藏");
            $("#a_collecthotel").parents(".hr_info_right").removeClass("colle2").addClass("colle1");
        }

    },
    //判断是否已登录
    JudgeIsLogin: function () {
        var user = _G.GetCookie("_user");
        //return true;
        if (user != null) {
            user = JSON.parse(user);
            userid = user.userid;
            return true;
        }
        else {
            return false;
        }
    },
    loading: '<div style="text-align: center;" id="divHotelLoading" >'
                + '<img src="/Images/loading.gif"  style="border: none; width: 70px;"/>'
                + '<p style="padding: 10px;">正在查询,请稍候....</p>'
            + '</div>'
};

//获取URL参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}