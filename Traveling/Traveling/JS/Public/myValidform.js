﻿var Validform = {
    //公共验证规则
    datatype: {
        "qq": /[1-9][0-9]{4,}/,                                 //qq号
        "idCode": /^(\d{18,18}|\d{15,15}|\d{17,17}X)$/,        //身份证
        "*": /[\w\W]+/,                                         //不能为空！
        "*6-16": /^[\w\W]{6,16}$/,                              //请填写6到16位任意字符！
        "n": /^\d+$/,                                           //数字
        "n6-16": /^\d{6,16}$/,                                  //请填写6到16位数字
        "s": /^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]+$/,           //不能输入特殊字符！
        "s6-18": /^[\u4E00-\u9FA5\uf900-\ufa2d\w\.\s]{6,18}$/,  //请填写6到18位字符！
        "p": /^[0-9]{6}$/,                                      //邮政编码
        "m": /^13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|16[0-9]{9}|17[0-9]{9}|18[0-9]{9}$/, //手机号
        "e": /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,   //邮箱
        "Pasport": /^1[45][0-9]{7}|G[0-9]{8}|P[0-9]{7}|S[0-9]{7,8}|D[0-9]+$/, //护照
        "Dl": /(\d{15})$/,                                        //驾驶证
        "Officer": /(\d{8})/,                                     //军官证
        "ph": /^(0[0-9]{2,3})?-([2-9][0-9]{6,7})+([0-9]{1,4})?$/,  //电话
        "n6": /\d{6}/,   //验证码
        "xm": /(^[\u4e00-\u9fa5]*$)|(^[A-Za-z]*[\\/][A-Za-z]+$){2,4}/, //姓名                               
        "nc": /^[\u4E00-\u9FA5a-zA-Z0-9_]{0,20}$/,                //昵称
        "f": /(^[\u4e00-\u9fa5]*$)|(^[A-Za-z]*[\\/][A-Za-z]+$)/,  //发票
        "pwd": /(?!^\\d+$)(?!^[a-zA-Z]+$)(?!^[_#@]+$).{8,18}/     //密码
    },
    formCheck: function (options) {
        var defaults = {
            valiContent: "",
            submitBtn: "",
            Tipmsg: "1",
            isAjax: true,
            ischeckBtn: "",
            passwordStrength: "#passwordStrength",
            valiType: "",
            errorMsgFn: function (pointer, alertMsg) { },
            myCallback: function () { }
        };
        options = $.extend({}, defaults, options);
        //conclass是form的选择器，submitBtn是提交按钮选择器，ischeckBtn是选择可选验证开关选择器                                                          // fn是回调函数
        var ipt = $(options.valiContent).find("input[datatype], select[datatype]");
        //改变验证类型
        $(options.valiContent).on("change", "select[valiType]", function () {
            var nextThat = $(this).next("input[datatype]");
            nextThat.attr("datatype", $(this).find("option:selected").attr("datatype"));
            nextThat.attr("errormsg", $(this).find("option:selected").text() + "格式错误");
            nextThat.attr("nullmsg", $(this).find("option:selected").text() + "不能为空");
            nextThat.trigger("blur");
        });
        //提交循环验证
        $(options.valiContent).on("click", options.submitBtn, function (event) {
            var len = ipt.length;
            ipt.each(function () {
                $(this).trigger("blur");
                if ($(this).hasClass("valiform-error")) {
                    return false;
                };
            });
            if ($(options.valiContent).find(".valiform-error").length == 0 && options.isAjax) {        //是ajax提交，代码待定
                options.myCallback();                                                              // 执行fn回调函数
                event.preventDefault();
            } else if ($(options.valiContent).find(".valiform-error").length == 0 && !options.isAjax) { // form提交
                $(options.valiContent).trigger("submit");
                event.preventDefault();
            } else {
                event.preventDefault();
            };
        });
        //验证输入框非空
        $(options.valiContent).on("blur", "input[datatype]", function () {
            var that = this;
            if (!$(options.ischeckBtn).prop("checked") && $(this).attr("ischeck") != undefined) {
                return false;
            };
            if ($(this).val() == "" && $(this).attr("ignore") == undefined) {
                if ($(this).attr("nullmsg") != undefined) {
                    $(this).addClass("valiform-error");
                    if (options.Tipmsg) {
                        $(this).next("span.text-error").removeClass("text-suc").text($(this).attr("nullmsg"));
                    } else {
                        options.errorMsgFn(that, $(this).attr("nullmsg"));
                    };
                } else {
                    $(this).addClass("valiform-error");
                    if (options.Tipmsg) {
                        $(this).next("span.text-error").text("请输入内容");
                    } else {
                        options.errorMsgFn(that, "请输入内容");
                    };
                };
            };
        });
        //验证输入框格式
        $(options.valiContent).on("blur", "input[datatype]", function () {
            var that = this;
            if (!$(options.ischeckBtn).prop("checked") && $(this).attr("ischeck") != undefined) {
                return false;
            };
            var re = eval(Validform.datatype[$(this).attr("datatype")]);
            if ($(this).val() == "") {
                return;
            };
            if (re.test($(this).val()) && $(this).val().search(re) == 0) {
                $(this).removeClass("valiform-error");
                $(this).next("span.text-error").addClass("text-suc").text("");
            } else {
                if ($(this).attr("errormsg") != undefined) {
                    $(this).addClass("valiform-error");
                    if (options.Tipmsg) {
                        $(this).next("span.text-error").removeClass("text-suc").text($(this).attr("errormsg"));
                    } else {
                        options.errorMsgFn(that, $(this).attr("errormsg"));
                    };
                } else {
                    $(this).addClass("valiform-error");
                    if (options.Tipmsg) {
                        $(this).next("span.text-error").removeClass("text-suc").text("格式错误");
                    } else {
                        options.errorMsgFn(that, "格式错误");
                    };
                };
            };
        });
        //验证非必填
        $(options.valiContent).on("blur", "input[datatype]", function () {
            if (!$(options.ischeckBtn).prop("checked") && $(this).attr("ischeck") != undefined) {
                return false;
            };
            if ($(this).val() == "" && $(this).attr("ignore") != undefined) {
                $(this).removeClass("valiform-error");
                $(this).next("span").text("");
            };
        });
        //选择验证
        $(options.valiContent + " " + options.ischeckBtn).click(function () {
            if (!$(this).prop("checked")) {
                $(options.valiContent).find("input[ischeck]").val("");
                $(options.valiContent).find("input[ischeck]").removeClass("valiform-error").next("span").text("");
                $(options.valiContent).find("select[ischeck]").removeClass("valiform-error").next("span").text("");
            };
        });
        //验证密码强度
        $(options.valiContent).on("keyup", " input:password:not([recheck])", function () {
            if ($(this).attr("passwordStrength") == "passwordStrength" && $(this).attr("type") == "password" && $(this).attr("recheck") == undefined && $(this).val().length > 5) {
                var passClass = 0;
                if ($(this).val().search(/[A-z]/) > -1) {
                    passClass += 1;
                };
                if ($(this).val().search(/[0-9]/) > -1) {
                    passClass += 1;
                };
                if ($(this).val().search(/\W/) > -1) {
                    passClass += 1;
                };
                $(options.passwordStrength + " span:lt(" + passClass + ")").addClass("bgStrength");
            } else if ($(this).attr("recheck") == undefined) {
                $(options.passwordStrength + " span").removeClass("bgStrength");
            };
        });
        //验证密码是否一致
        $(options.valiContent).on("blur", "input[recheck]", function () {
            var that = this;
            if ($(this).val() != $(options.valiContent + " input[name=" + $(this).attr("recheck") + "]").val() && $(this).val() != "") {
                if ($(this).attr("nullmsg") != undefined) {
                    $(this).addClass("valiform-error");
                    if (options.Tipmsg) {
                        $(this).next("span").removeClass("text-suc").text($(this).attr("errormsg"));
                    } else {
                        options.errorMsgFn(that, $(this).attr("errormsg"));
                    };
                };
            } else if ($(this).val() == $(options.valiContent + " input[name=" + $(this).attr("recheck") + "]").val() && $(this).val() != "") {
                $(this).removeClass("valiform-error");
                $(this).next("span").text("");
            };
        });
        //验证下拉框
        $(options.valiContent).on("blur", "select[datatype]", function () {
            var that = this;
            if ($(this).find("option:selected").text().indexOf("请选择") > -1 && $(this).attr("ischeck") == undefined) {
                $(this).addClass("valiform-error");
                $(this).next("span").removeClass("text-suc");
                if ($(this).attr("errormsg") == undefined) {
                    if (options.Tipmsg) {
                        $(this).next("span").text("格式错误");
                    } else {
                        options.errorMsgFn(that, "格式错误");
                    };
                } else {
                    if (options.Tipmsg) {
                        $(this).next("span").text($(this).attr("errormsg"));
                    } else {
                        options.errorMsgFn(that, $(this).attr("errormsg"));
                    };
                }
            } else {
                $(this).removeClass("valiform-error");
                $(this).next("span").addClass("text-suc").text("");
            }
        });
        //选择验证下拉框
        $(options.valiContent).on("blur", "select[datatype][ischeck]", function () {
            var that = this;
            if ($(this).find("option:selected").text().indexOf("请选择") > -1 && $(this).attr("ischeck") == "ischeck" && $(options.ischeckBtn).prop("checked")) {
                $(this).addClass("valiform-error");
                $(this).next("span").removeClass("text-suc");
                if ($(this).attr("errormsg") == undefined) {
                    if (options.Tipmsg) {
                        $(this).next("span").text("格式错误");
                    } else {
                        options.errorMsgFn(that, "格式错误");
                    };
                } else {
                    if (options.Tipmsg) {
                        $(this).next("span").text($(this).attr("errormsg"));
                    } else {
                        options.errorMsgFn(that, $(this).attr("errormsg"));
                    };
                }
            } else {
                $(this).removeClass("valiform-error");
                $(this).next("span").addClass("text-suc").text("");
            }
        });
    },
    reset: function (content) {
        var ipt = $(content).find("input[datatype], select[datatype]");
        ipt.val("").removeClass("valiform-error");
        ipt.next("span").text("");
    }
};