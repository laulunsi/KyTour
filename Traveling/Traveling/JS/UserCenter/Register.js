﻿jQuery.support.cors = true;
var mapdata = {
    DataTimer: null
};
$(function () {
    //GetLocalIP();
    //GetValidateCode();
    BindOnClick();
    FormValid(); // 表单验证
})
//绑定动态事件
function BindOnClick() {
    //同意条款
    $("#btn_AgreeItem").click(function () {
        if ($("#btn_AgreeItem").prop("checked")) {
            $("#btn_Register").addClass('active');
        }
        else {
            $("#btn_Register").removeClass('active');
        }
    });
    $("#img_ValidateCode").bind("click", function () {
        $(this).attr("src", "/Public/VerifyCode.aspx?t=" + (new Date()).getTime());//随机数保证验证码不缓存

    });
}
// 表单验证
function FormValid() {
    //验证-- 用户注册 
    Validform.formCheck({
        valiContent: "#Form_Register",      //容器盒子，注意：自己定义
        submitBtn: "#btn_Register",   //提交按钮， 注意：自己定义
        ischeckBtn: "", //选择按钮，控制下边表单是否验证
        isAjax: true,           //指定是否ajax，默认是true。
        valiType: ".valiType",  //下拉列表框选择验证类型
        myCallback: function () {  //回调函数
            if ($("#btn_AgreeItem").prop("checked")) {
                //重复点击时不发送请求
                clearTimeout(mapdata.DataTimer);
                mapdata.DataTimer = setTimeout(function () {
                    CheckUserName();
                }, 300);
            }
        }
    });

    $("#reg_mobile").blur(function () {
        var reg_mobile = $('#reg_mobile').val();
        //验证用户名
        post("base_checkuser", { "username": reg_mobile }, BindCheckUser);
        function BindCheckUser(result) {
            if (result.status == 1 && result.data.code == "u002") {
                $('#err_mobile').text("手机号已存在！").attr('class', 'text-error');
                return;
            }
            else
                $('#err_mobile').text("");
        }
    });
    $("#reg_code").blur(function () {
        var reg_code = $('#reg_code').val();
        //验证随机码是否正确
        commonAjax("action=getverifycode", GetVerifyCode);
        function GetVerifyCode(data) {
            if (reg_code.toLowerCase() != data.toLowerCase()) {
                $('#err_code').text("验证码不正确！").attr('class', 'text-error');
                return;
            }
            else
                $('#err_code').text("");
        }
    });
}
////获取本址IP
//function GetLocalIP()
//{
//    $.post("/Common/GetLocalIP", function (data)
//    {
//        global.IP = data.ip;
//    });
//}
////获取随机验证码
//function GetValidateCode()
//{
//    $.post("/Common/CreateValidateCode", function (data)
//    {
//        global.SessionCode = data;
//        $("#img_ValidateCode").attr('src', "/Common/GetValidateCode?code=" + global.SessionCode + "&time=" + (new Date()).getTime());
//    });  
//}
//用户注册
function CheckUserName() {
    try {
        var sendMsgFlag = true;
        var reg_mobile = $('#reg_mobile').val();
        //var reg_email = $('#reg_email').val();
        var reg_pwd = $('#reg_pwd').val();
        var reg_code = $('#reg_code').val();
        ////验证用户名
        //post("base_checkuser", { "username": reg_mobile }, BindCheckUser);
        //function BindCheckUser(result)
        //{
        //    if (result.status == 1)
        //    {
        //        $('#err_mobile').text("手机号已存在！").attr('class', 'text-error');
        //        sendMsgFlag = false;
        //        return;
        //    }
        //}
        ////验证随机码是否正确
        //commonAjax("action=getverifycode", GetVerifyCode);
        //function GetVerifyCode(data)
        //{
        //    if (reg_code.toLowerCase() != data.toLowerCase())
        //    {
        //        $('#err_code').text("验证码不正确！").attr('class', 'text-error');
        //        sendMsgFlag = false;
        //        return;
        //    }
        //}
        //验证通过后，发送邮件
        if (sendMsgFlag) {
            SendMessage();
        }



        //var queryData = {
        //    "username": reg_mobile,
        //    "password": $.md5(reg_pwd),
        //    "randcode": "",
        //    "registertype": 1
        //};

        //post("base_registeruser", queryData, BindRegisterUser);
        ////绑定
        //function BindRegisterUser(result)
        //{
        //    if (result.status == 1)
        //    {              
        //        //SendMessage();

        //        _G.DeleteCookie("_registerinfo");
        //        _G.SetCookie("_registerinfo", JSON.stringify(result.data), 1, "d");
        //        location.href = "RegisterValidate.shtml";
        //    }
        //    else
        //    {
        //        $('#err_mobile').text(result.message).attr('class', 'text-error');
        //        return;
        //    }
        //}

    } catch (e)
    { }
}
// 发送短信
function SendMessage() {
    try {
        var reg_mobile = $("#reg_mobile").val();
        var queryData = {
            "recipient": reg_mobile,
            "smstype": 2,
            "content": "",
            "title": ""
        }

        post("base_sendsms", queryData, BindSendSMSCode);
        function BindSendSMSCode(result) {
            if (result.status == 1) {
                var reg_pwd = $('#reg_pwd').val();
                var registerinfo = {
                    "username": reg_mobile,
                    "password": $.md5(reg_pwd),
                };
                _G.DeleteCookie("_registerinfo");
                _G.SetCookie("_registerinfo", JSON.stringify(registerinfo), 1, "d");
                location.href = "RegisterValidate.shtml";
            }
            else {
                alert(result.message);
            }
        }
    } catch (e)
    { }
}
