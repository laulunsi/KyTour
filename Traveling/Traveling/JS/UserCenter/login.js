﻿// 构造登录类
function loginClass() {
    //_G.DeleteCookie("_user");
};
loginClass.prototype.callback = null;
loginClass.prototype.tab = function (tit, con) {
    $(con).hide();
    $(con).eq(0).show();
    $(document).on("click", tit, function () {
        $(this).addClass("active").siblings().removeClass("active");
        var index = $(this).index();
        $(con).eq(index).show().siblings(".com-ul-login-con").hide();
    });
};
loginClass.prototype.comlogin = function () {
    //正常登录
    $("body").on("click", "#btn_Login", function () {

        var name = $("#Login_Name").val();
        var pwd = $("#Login_PWD").val();
        if (name == "") {
            $("#Login_errMsg").html("登录名不能为空！").show();
            return;
        }
        if (pwd == "") {
            $("#Login_errMsg").html("密码不能为空！").show();
            return;
        }
        var queryData = {
            "username": name,
            "password": $.md5(pwd),
            "randcode": ""
        }
        _loginFn.UserLogin(queryData, "#Login_errMsg");
    })
    //快捷登录
    $("body").on("click", "#btn_QuickLogin", function () {
        var mobile = $("#Login_Mobile").val();
        var code = $("#Login_Code").val();
        if (mobile == "") {
            $("#QuickLogin_errMsg").html("手机号不能为空！").show();
            return;
        }
        if (code == "") {
            $("#QuickLogin_errMsg").html("动态码不能为空！").show();
            return;
        }
        var queryData = {
            "username": mobile,
            "password": "",
            "randcode": code
        }
        _loginFn.UserLogin(queryData, "#QuickLogin_errMsg");
    })
    //发送短信
    $("body").on("click", "#btn_SendCode", function () {
        if ($(this).hasClass("disable")) {
            return;
        }
        _loginFn.SendSMSCode();
    })

    //回车-普通登录
    $("body").on("keydown", "#Login_Name,#Login_PWD", function (e) {
        var key = e.which;
        if (key == 13) {
            $("#btn_Login").click();
        }
    })
    //回车-普通登录
    $("body").on("keydown", "#Login_Mobile,#Login_Code", function (e) {
        var key = e.which;
        if (key == 13) {
            $("#btn_QuickLogin").click();
        }
    })

};
// 构造tankuang通用登录类
function ComloginClass() {
    loginClass.call(this); //继承登录类
};
ComloginClass.prototype = new loginClass(); //继承登录类切换方法
ComloginClass.prototype.init = function (url, status) {
    if (!$(".loginBg").length) {
        var str = '<div class = "loginBg loginBg1"></div><div class="com-login-box com-login-box1"><div class ="icon-arr"><span id = "closeLogin">关闭</span></div><div class="login-tit clearfix">';
        str += '<div class="com-login active">普通登录</div><div class="ph-login">手机动态码登录</div></div>';
        str += '<div class="com-ul-login-con" style="display: block;"><ul>';
        str += '<li><span class="label">登录名</span><input id="Login_Name" type="text" placeholder="登录名"></li>';
        str += '<li><span class="label">密　码</span><input id="Login_PWD" type="password"> <a class="lg-forget-pwd" href="/UserCenter/ResetInfo.shtml">忘记密码</a></li>';
        str += '<li><label id="Login_errMsg" class="lab_errMsg"></label></li>';
        str += '<li><span class="label"></span><input id="btn_Login" class="submit" type="button" value="登录"></li>';
        str += '</ul></div>';
        str += '<div class="com-ul-login-con" style="display: none;"><ul>';
        str += '<li><span class="label">手机号</span><input id="Login_Mobile" type="text" placeholder="登录名"></li>';
        str += '<li><span class="label">动态码</span><input id="Login_Code" type="text" style="width:100px;"> <input id="btn_SendCode" class="reSend" type="button" value="获取动态码"> <input id="btn_SendTimer" class="reSend reSendTimer" type="button" value="60秒" disabled="disabled"></li>';
        str += '<li><label id="QuickLogin_errMsg" class="lab_errMsg"></label></li>';
        str += '<li><span class="label"></span><input id="btn_QuickLogin" class="submit" type="button" value="登录"></li>';
        str += '</ul></div><div id ="goRegister" class = "go-reg">您还没有你定旅行网帐号，<a href="/UserCenter/Register.shtml" class="lg-forget-pwd">免费注册</a></div><a href="" id ="btn_GoToBuy" class="img-reseve">不登录，直接预订</a></div>';
        $("body").append(str);
        $("head").append('<link id = "comLogin" rel="stylesheet" type="text/css" href="">');
        if (status == 1)//登录
        {
            $("#goRegister, #btn_GoToBuy").hide();
        }
        else if (status == 2)//登录 +  注册
        {
            $(" #btn_GoToBuy").hide();
        }
        else if (status == 3)//登录 +  注册 +  直接预订
        {

        }


        $("#comLogin").attr("href", url);
        $(document).on("click", "#closeLogin", function () {
            $(".loginBg").addClass("loginBg1")
            $(".com-login-box").addClass("com-login-box1")
        });
        ComloginClass.prototype.tab(".login-tit>div", ".com-ul-login-con");
        ComloginClass.prototype.comlogin();
    }
};
ComloginClass.prototype.showComlogin = function () {
    $(".loginBg").removeClass("loginBg1");
    $(".com-login-box").removeClass("com-login-box1");
};
ComloginClass.prototype.hideComlogin = function () {
    $(".loginBg").removeClass("loginBg1");
    $(".com-login-box").removeClass("com-login-box1");
};

//用户登录
var _loginFn = {
    //用户登录
    UserLogin: function (queryData, err_com) {
        post("base_login", queryData, BindLoginDate);
        function BindLoginDate(result) {
            if (result.status == 1) {
                //var userinfo = result.data;
                //var myuser = { userid: userinfo.id, username: userinfo.username, cid: userinfo.cid };
                //_G.DeleteCookie("_user");
                //_G.SetCookie("_user", JSON.stringify(myuser), 1, "d");   
                if (loginClass.prototype.callback != null) {
                    loginClass.prototype.callback();
                }
                else {
                    var url = $("#btn_GoToBuy").attr("href");
                    if (url != null || url != "") {
                        location.href = $("#btn_GoToBuy").attr("href");
                    }
                }
            }
            else {
                $(err_com).text(result.message);
            }
        }
    },

    //发送手机验证码
    SendSMSCode: function () {
        var queryData = {
            "recipient": $("#Login_Mobile").val(),
            "smstype": 1,
            "content": "",
            "title": ""
        }

        post("base_sendsms", queryData, BindSendSMSCode);
        function BindSendSMSCode(result) {
            if (result.status == 1) {
                Timer("#btn_SendCode", "#btn_SendTimer", 10, 1 * 60);
            }
            else {
                alert(result.message);
            }
        }
    }
};


//检测是否登录  
//方法：CheckUserLogin(linkurl, type, status,callback); 
//参数说明： linkurl== 要跳转的页面 
//           type== target：表示新窗口中打开跳转页面 。 在当前页打开为""
//           status==1：登录     2：登录+注册    3：登录+注册+直接预订
function CheckUserLogin(linkurl, type, status, callback) {
    loginClass.prototype.callback = callback;
    var userinfo = _G.GetCookie("_user");
    if (userinfo != null) {
        if (type == "target") {
            window.open(linkurl, "_blank");
        } else {
            if (loginClass.prototype.callback != null) {
                loginClass.prototype.callback();
            }
            else
                location.href = linkurl;
        }
    }
    else {
        var loginCom = new ComloginClass();
        loginCom.init("../CSS/UserCenter/comlogin.css", status);//登录窗口初始化
        loginCom.showComlogin(); //调用登录框

        if (callback != null) {
            $("#btn_GoToBuy").removeAttr('href');
            $(document).on("click", "#btn_GoToBuy", function () {
                callback();
            });
        }
        else {
            $("#btn_GoToBuy").attr('href', linkurl);
            if (type == "target") {
                $("#btn_GoToBuy").attr('target', "_blank");
            }
        }
    }
};