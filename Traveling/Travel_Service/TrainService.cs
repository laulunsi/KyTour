﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_Model;
using Travel_DAL;

namespace Travel_Service
{
    public class TrainService
    {
        Train_DAL dal = new Train_DAL();
        /// <summary>
        /// 显示座位类别表
        /// </summary>
        /// <returns></returns>
        public List<seattype> showseattype()
        {
            return dal.showseattype();
        }
        /// <summary>
        /// 显示车次类型
        /// </summary>
        /// <returns></returns>
        public List<trainnumbertype> showtrainnumbertype()
        {
            return dal.showtrainnumbertype();
        }
        /// <summary>
        /// 显示车次信息
        /// </summary>
        /// <returns></returns>
        public List<trainnumber> showtrainnumber()
        {
            return dal.showtrainnumber();
        }
        /// <summary>
        /// 显示时段信息
        /// </summary>
        /// <returns></returns>
        public List<timeframe> showtimeframe()
        {
            return dal.showtimeframe(); ;
        }
        /// <summary>
        /// 显示城市信息
        /// </summary>
        /// <returns></returns>
        public List<city> showcity()
        {
            return dal.showcity();
        }
        /// <summary>
        /// 显示车站信息
        /// </summary>
        /// <returns></returns>
        public List<station> showstation()
        {
            return dal.showstation(); ;
        }
        /// <summary>
        /// 绑定证件信息下拉框
        /// </summary>
        /// <returns></returns>
        public List<papersType> showpapersType()
        {
            Train_DAL dal = new Train_DAL();
            return dal.showpapersType();
        }
        /// <summary>
        /// 显示车次表信息
        /// </summary>
        /// <returns></returns>
        public List<trainnumber> Trainnumbershow()
        {
            return dal.Trainnumbershow();
        }
        /// <summary>
        /// 删除车次表信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int trainnumberDelete(int id)
        {
            return dal.trainnumberDelete(id);
        }
        /// <summary>
        /// 添加车次信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int trainnumberInsert(Travel_Model.trainnumber model)
        {
            return dal.trainnumberInsert(model);
        }
        /// <summary>
        /// 通过id找到要修改的行
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public trainnumber FindTrainnumberInforById(int id)
        {
            return dal.FindTrainnumberInforById(id);
        }
        /// <summary>
        /// 实现修改功能
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int trainnumberUpdate(Travel_Model.trainnumber model)
        {
            return dal.trainnumberUpdate(model);
        }
        /// <summary>
        /// 按照Trainname搜索车次信息
        /// </summary>
        /// <param name="Trainname"></param>
        /// <returns></returns>
        public List<trainnumber> SelectTrainnumberByTrainname(string Trainname)
        {
            return dal.SelectTrainnumberByTrainname(Trainname);
        }
    }
}
