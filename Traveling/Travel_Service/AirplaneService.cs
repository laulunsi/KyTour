﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_DAL;
using Travel_Model;
namespace Travel_Service
{
    public class AirplaneService
    {
        Airplane_DAL dal = new Airplane_DAL();
        
        /// <summary>
       /// 航空公司显示
       /// </summary>
       /// <returns></returns>
        public List<airport> AirlineShwo()
        {
            return dal.AirlineShwo();
        }

         /// <summary>
       /// 机票显示
       /// </summary>
       /// <returns></returns>
        public List<airticket> airticketShow()
        {
            return dal.airticketShow();
        }

    }
}
