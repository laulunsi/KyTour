﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_DAL;
using Travel_Model;

namespace Travel_Service
{
    public class HotelService
    {
        Hotel_DAL dal = new Hotel_DAL();
        /// <summary>
        /// 显示页面
        /// </summary>
        /// <returns></returns>
        public List<hotel> hotelshow()
        {
            return dal.hotelshow();
        }
    }
}
