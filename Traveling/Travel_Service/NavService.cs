﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_DAL;
using Travel_Model;

namespace Travel_Service
{
   public  class NavService
    {
       Navdal dal = new Navdal();
        /// <summary>
        /// 显示导航信息
        /// </summary>
        /// <returns></returns>
        public List<nav> navshow()
        {
            return dal.navshow();
        }
        /// <summary>
        /// 显示轮播图片
        /// </summary>
        /// <returns></returns>
        public List<advertisement> showimg()
        {
            return dal.showimg();
        }
    }
}
