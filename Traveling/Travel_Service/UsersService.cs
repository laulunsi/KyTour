﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_DAL;
using Travel_Model;

namespace Travel_Service
{
   public  class UsersService
    {
       Users_DAL dal = new Users_DAL();
        /// <summary>
        /// 判断数据库有没有记录
        /// </summary>
        /// <param name="uname"></param>
        /// <param name="upwd"></param>
        /// <returns></returns>
       public DataSet ListCount(string uname, string upwd)
        {
            return dal.ListCount(uname,upwd);
        }
        /// <summary>
        /// 显示用户类型
        /// </summary>
        /// <returns></returns>
        public DataSet showtype()
        {
            return dal.showtype();
        }
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="us"></param>
        /// <returns></returns>
        public int Add(userinfor us)
        {
            return dal.Add(us);
        }
        /// <summary>
        /// 判断用户明是否存在
        /// </summary>
        /// <param name="uname"></param>
        /// <returns></returns>
        public DataSet finduName(string uname)
        {
           
            return dal.finduName(uname);
        }
    }
}
