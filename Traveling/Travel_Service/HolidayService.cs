﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travel_Model;
using Travel_DAL;

namespace Travel_Service
{
    public class HolidayService
    {

       Holiday_DAL dal = new Holiday_DAL();
        /// <summary>
        /// 显示度假信息
        /// </summary>
        /// <returns></returns>
       public List<holiday> Showholiday(string Playline)
        {
            return dal.Showholiday(Playline);
        }


       /// <summary>
       /// 添加度假信息
       /// </summary>
       /// <param name="day"></param>
       /// <returns></returns>
       public int Addholiday(holiday day)
       {
           return dal.Addholiday(day);
       }

       /// <summary>
       /// 删除度假信息
       /// </summary>
       /// <param name="day"></param>
       /// <returns></returns>
       public int DelHoliday(int id)
       {
           return dal.DelHoliday(id);
       }


       /// <summary>
       /// 根据id修改行内容行
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public List<holiday> UpdholidayShow(int id)
       {
           return dal.UpdholidayShow(id);
       }



       /// <summary>
       /// 修改度假信息
       /// </summary>
       /// <param name="day"></param>
       /// <returns></returns>
       public int UpdHoliday(holiday day)
       {
           return dal.UpdHoliday(day);
       }

        /// <summary>
        /// 费用说明
        /// </summary>
        /// <returns></returns>
        public List<Coitexplai> Coitexplain_Show()
        {
            return dal.Coitexplain_Show();
        }
        /// <summary>
        /// 订单须知
        /// </summary>
        /// <returns></returns>
        public List<Ordernotict> Ordernotict_Show()
        {
            return dal.Ordernotict_Show();
    }
        /// <summary>
        /// 签证说明
        /// </summary>
        /// <returns></returns>
        public List<Visaexplain> Visaexplain_Show()
        {
            return dal.Visaexplain_Show();
        }
        /// <summary>
        /// 退款说明
        /// </summary>
        /// <returns></returns>
        public List<Refundexplain> Refundexplain_Show()
        {
            return dal.Refundexplain_Show();
}
        /// <summary>
        /// 补充说明
        /// </summary>
        /// <returns></returns>
        public List<Elseexplain> Elseexplain_Show()
        {
            return dal.Elseexplain_Show();
        }
        /// <summary>
        /// 度假评分表
        /// </summary>
        /// <returns></returns>
        public List<grade> grade_Show()
        {
            return dal.grade_Show();
        }
        /// <summary>
        /// 游玩人类别显示
        /// </summary>
        /// <returns></returns>
        public List<amuseoneself> amuseoneself_Show()
        {
            return dal.amuseoneself_Show();
        }
        /// <summary>
        /// 行程介绍
        /// </summary>
        /// <returns></returns>
        public List<journeyreferral> journeyreferral_Show()
        {
            return dal.journeyreferral_Show();
        }
        /// <summary>
        /// 行程天数
        /// </summary>
        /// <returns></returns>
        public List<journeyday> journeyday_Show()
        {
            return dal.journeyday_Show();
        }
        /// <summary>
        /// 度假显示
        /// </summary>
        /// <returns></returns>
        public List<holiday> Holiday_Show()
        {
            return dal.Holiday_Show();
        }
    }
}


