﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travel_Service;
using Travel_Model;

namespace TravelAdmin.Controllers
{

    public class AstartController : Controller
    {
        HolidayService service = new HolidayService();
        TrainService trainservice = new TrainService();
        //
        // GET: /Home/

        /// <summary>
        /// 登录
        /// </summary>
        /// <returns></returns>
        public ActionResult Admin()
        {
            return View();
        }
        /// <summary>
        /// 用户信息的维护
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 导航与广告轮播的数据维护
        /// </summary>
        /// <returns></returns>
        public ActionResult NavMenage()
        {
            return View();
        }
        /// <summary>
        /// 飞机票的信息维护
        /// </summary>
        /// <returns></returns>
        public ActionResult AirMenage()
        {
            return View();
        }
        /// <summary>
        /// 火车票的信息维护
        /// </summary>
        /// <returns></returns>
        public ActionResult TrainMenage()
        {
            return View();
        }
        /// <summary>
        /// 显示所有的车次信息
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowTrainnumberInfor()
        {
            List<trainnumber> list = trainservice.Trainnumbershow();
            return Json(list, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 添加车次信息视图
        /// </summary>
        /// <returns></returns>
        public ActionResult AddTrainnumberInfor()
        {
            return View();
        }
        /// <summary>
        /// 添加车次信息
        /// </summary>
        /// <returns></returns>
        public int AddTrainnumberInforDo(trainnumber train)
        {
            train.Trainname = Request["Trainname"].ToString();
            train.Starttime = Request["Starttime"].ToString();
            train.Endtime = Request["Endtime"].ToString();
            train.Stop = Request["Stop"].ToString();
            train.Stoptime = Request["Stoptime"].ToString();
            train.operation = Request["operation"].ToString();
            train.Farenumber = Convert.ToInt32(Request["Farenumber"].ToString());
            if (trainservice.trainnumberInsert(train) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }
        /// <summary>
        /// 删除车次信息
        /// </summary>
        /// <returns></returns>
        public int DeleteTrainnumberInfor(int id)
        {
            
            if (trainservice.trainnumberDelete(id) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 显示修改视图
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FindTrainnumberInfor()
        {           
            return View();
        }
        /// <summary>
        /// 通过id显示车次信息
        /// </summary>
        /// <returns></returns>
        public ActionResult Eidt_show()
        {
            int id = Convert.ToInt32(Request["Trainnumberid"]);
            trainnumber list = trainservice.FindTrainnumberInforById(id);
            return Json(list,JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 实现修改功能
        /// </summary>
        /// <returns></returns>
        public int UpdateTrainnumberInforDo(trainnumber train)
        {
            train.Trainname = Request["Trainname"].ToString();
            train.Starttime = Request["Starttime"].ToString();
            train.Endtime = Request["Endtime"].ToString();
            train.Stop = Request["Stop"].ToString();
            train.Stoptime = Request["Stoptime"].ToString();
            train.operation = Request["operation"].ToString();
            train.Farenumber = Convert.ToInt32(Request["Farenumber"].ToString());
            train.Trainnumberid = Convert.ToInt32(Request["Trainnumberid"].ToString());
            if (trainservice.trainnumberUpdate(train) > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 度假信息的维护
        /// </summary>
        /// <returns></returns>
        public ActionResult HolMenage()
        {
            return View();
        }
        /// <summary>
        /// 酒店信息的维护
        /// </summary>
        /// <returns></returns>
        public ActionResult HoltialMenage()
        {
            return View();
        }

        /// <summary>
        /// 度假信息展示
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ShowShowholiday(string Search)
        {
            List<holiday> list = service.Showholiday(Search);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        



    }
}
