﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Travel_Model;
using System.Data;

namespace Travel_DAL
{
   public  class Navdal
    {
       /// <summary>
       /// 连接数据库
       /// </summary>
       Database db = DatabaseFactory.CreateDatabase("Connection");
       /// <summary>
       /// 显示导航信息
       /// </summary>
       /// <returns></returns>
       public List<nav> navshow()
       {
           string str = "SELECT top (6) * FROM nav";

            DbCommand cmd = db.GetSqlStringCommand(str);

            List<nav> list = new List<nav>();

            IDataReader da = db.ExecuteReader(cmd);

            while (da.Read())

            {
                nav na = new nav();
                na.id = Convert.ToInt32(da["id"]);
                na.navname = da["navname"].ToString();
                na.navurl = da["navurl"].ToString();
                list.Add(na);
            }

            return list;
       }
       /// <summary>
       /// 显示轮播图片
       /// </summary>
       /// <returns></returns>
       public List<advertisement> showimg()
       {
           string str = "SELECT * FROM advertisement";

           DbCommand cmd = db.GetSqlStringCommand(str);

           List<advertisement> list = new List<advertisement>();

           IDataReader da = db.ExecuteReader(cmd);

           while (da.Read())
           {
               advertisement adv = new advertisement();
               adv.id = Convert.ToInt32(da["id"]);
               adv.Imgurl = da["Imgurl"].ToString();
               adv.url = da["url"].ToString();
               list.Add(adv);
           }

           return list;
       }
    }
}
