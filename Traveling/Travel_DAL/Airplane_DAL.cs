﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Travel_Model;
using System.Data.Common;
namespace Travel_DAL
{
   public class Airplane_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Connection");//连接企业数据库
       /// <summary>
       /// 航空公司显示
       /// </summary>
       /// <returns></returns>
        public List<airport> AirlineShwo()
        {
            string sql = "select c.Startairport,c.Endairport, a.Takeofftime,b.Adult from airticket a join Price b on a.id=b.id  join airport c on a.Airportid=c.Airlineid ";
            DbCommand comm = db.GetSqlStringCommand(sql);
            comm.Connection = db.CreateConnection();
            List<airport> list = new List<airport>();
            DbDataAdapter adapter = db.GetDataAdapter();
            adapter.SelectCommand = comm;
            DataTable dt = new DataTable();
            adapter.Fill(dt) ;
            foreach (DataRow i in dt.Rows)
            {
                airport a = new airport();
                //a.id = Convert.ToInt32(i["id"]);
                //a.Airlineid = Convert.ToInt32(i["Airlineid"]);
                a.Endairport = i["Endairport"].ToString();
                //a.Photo = i["Photo"].ToString();
                a.Startairport = i["Startairport"].ToString();
                a.Takeofftime = Convert.ToDateTime(i["Takeofftime"]).ToString("MM-dd");
               
                a.Adult = i["Adult"].ToString();
                list.Add(a);
            }
            return list;
        }
       /// <summary>
       /// 机票显示
       /// </summary>
       /// <returns></returns>
        public List<airticket> airticketShow()
        {
            string sql = "select a.*,b.Endairport,b.Startairport from  airticket a  join airport b on a.Airportid=b.Airlineid";
            DbCommand com = db.GetSqlStringCommand(sql);
            com.Connection = db.CreateConnection();
            List<airticket> list = new List<airticket>();
            DbDataAdapter da = db.GetDataAdapter();
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow i in dt.Rows)
            {
                airticket a = new airticket();
                a.Airportid = Convert.ToInt32(i["Airportid"]);
                a.Arrivaltiem =Convert.ToDateTime( i["Arrivaltiem"]);
                a.Discount = i["Discount"].ToString();
                a.id = Convert.ToInt32(i["id"]);
                a.seat = i["seat"].ToString();
                a.Stop = Convert.ToInt32(i["Stop"]);
                a.Takeofftime = Convert.ToDateTime(i["Takeofftime"]);
                a.yeornocaterint = i["yeornocaterint"].ToString();
                list.Add(a);
                
            }
            return list;
        }
            
      
    }
}
