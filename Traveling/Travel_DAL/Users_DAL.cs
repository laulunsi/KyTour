﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Travel_Model;
using System.Data;
using System.Xml;

namespace Travel_DAL
{
   public class Users_DAL
    {
        /// <summary>
        /// 连接数据库
        /// </summary>
        
        Database db = DatabaseFactory.CreateDatabase("Connection");
       /// <summary>
       /// 判断数据库有没有记录
       /// </summary>
       /// <param name="uname"></param>
       /// <param name="upwd"></param>
       /// <returns></returns>
        public DataSet ListCount(string uname, string upwd)
        {
            List<userinfor> list = new List<userinfor>();
            string sql = "select * from userinfor where username='"+uname+"' and userpwd='"+upwd+"'";
            DataSet ds = db.ExecuteDataSet(CommandType.Text, sql);
            return ds;
        }
       /// <summary>
       /// 显示用户类型
       /// </summary>
       /// <returns></returns>
       public DataSet showtype()
       {
           string str = "SELECT * FROM user_type";
           DataSet ds = db.ExecuteDataSet(CommandType.Text, str);
           return ds;
       }
       /// <summary>
       /// 注册用户
       /// </summary>
       /// <param name="us"></param>
       /// <returns></returns>
       public int Add(userinfor us)
       {
           string sql = "insert into userinfor values(@username,@userpwd,@userphone,@usertype)";
           DbCommand cmd = db.GetSqlStringCommand(sql);
           db.AddInParameter(cmd, "@username", DbType.String, us.username);
           db.AddInParameter(cmd, "@userpwd", DbType.String, us.userpwd);
           db.AddInParameter(cmd, "@userphone", DbType.String, us.userphone);
           db.AddInParameter(cmd, "@usertype", DbType.Int32, us.usertype);
           return db.ExecuteNonQuery(cmd);
       }
       /// <summary>
       /// 判断用户明是否存在
       /// </summary>
       /// <param name="uname"></param>
       /// <returns></returns>
       public DataSet finduName(string uname)
       {
           userinfor user = new userinfor();
           string sql = "select * from userinfor where username='" + uname + "'";
           DataSet ds = db.ExecuteDataSet(CommandType.Text,sql);
           return ds;
       }
    }
}
