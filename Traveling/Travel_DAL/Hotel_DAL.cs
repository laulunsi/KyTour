﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Travel_Model;
using System.Data;

namespace Travel_DAL
{
    public class Hotel_DAL
    {
        Database db = DatabaseFactory.CreateDatabase("Connection");//连接数据库
        /// <summary>
        /// 显示页面
        /// </summary>
        /// <returns></returns>
        public List<hotel> hotelshow()
        {
            string str = "SELECT * FROM hotel";

            DbCommand cmd = db.GetSqlStringCommand(str);
            List<hotel> list = new List<hotel>();
            IDataReader da = db.ExecuteReader(cmd);
            while (da.Read())
            {
                hotel h = new hotel();
                h.id = Convert.ToInt32(da["id"]);
                h.hoteladdress = da["hoteladdress"].ToString();
                h.hotelname = da["hotelname"].ToString();
                h.intime = da["intime"].ToString();
                h.outtime = da["outtime"].ToString();
                h.hotelImg = da["hotelImg"].ToString();
                h.hotelrank = da["hotelrank"].ToString();
                h.hotelintroduce = da["hotelintroduce"].ToString();
                list.Add(h);
            }
            return list;
        }
    }
}
