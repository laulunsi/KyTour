﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 签证说明表
    /// </summary>
    public  class Visaexplain
    {
        /// <summary>
        /// 签证id
        /// </summary>
        public int Visaexplainid { get; set; }
        /// <summary>
        /// 签证说明
        /// </summary>
        public string Visaexplains { get; set; }  
    }
}
