﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 酒店信息
    /// </summary>
    public class hotelmessage
    {
        /// <summary>
        /// 酒店编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 基本信息
        /// </summary>
        public string basicmessage { get; set; }
        /// <summary>
        /// 酒店设施
        /// </summary>
        public string Hotelfacility { get; set; }
        /// <summary>
        /// 酒店外键id
        /// </summary>
        public int Hotelid { get; set; }
    }
}
