﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 航空公司表
    /// </summary>
    public class airline
    {
        /// <summary>
        /// 航空公司id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 航空公司名称
        /// </summary>
        /// 
        public string Airlinename { get; set; }
      
    }
}
