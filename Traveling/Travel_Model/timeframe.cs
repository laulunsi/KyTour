﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 时段表
    /// </summary>
    public class timeframe
    {
        /// <summary>
        /// 时段编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 时段内容
        /// </summary>
        public string Timecontent { get; set; }
    }
}
