﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 补充说明表
    /// </summary>
    public class Elseexplain
    {
        /// <summary>
        /// 补充说明id
        /// </summary>
        public int Elseexplainid { get; set; }
        /// <summary>
        /// 补充说明
        /// </summary>
        public string Elseexplains { get; set; }
    }
}
