﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 导航表
    /// </summary>
    public class nav
    {
        /// <summary>
        /// 导航id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 导航名称
        /// </summary>
        public string navname { get; set; }
        /// <summary>
        /// 导航跳转路径
        /// </summary>
        public string navurl { get; set; }
    }
}
