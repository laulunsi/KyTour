﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 广告图片轮播表
    /// </summary>
    public class advertisement
    {
        /// <summary>
        /// 轮播编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string Imgurl { get; set; }
        /// <summary>
        /// 跳转地址
        /// </summary>
        public string url { get; set; }
    }
}
