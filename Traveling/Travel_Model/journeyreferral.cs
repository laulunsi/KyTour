﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 行程介绍
    /// </summary>
    public class journeyreferral
    {
        /// <summary>
        /// 行程id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 行程地区
        /// </summary>
        public string Jouneyregion { get; set; }
        /// <summary>
        /// 交通工具
        /// </summary>
        public string Trafficvehicle { get; set; }
        /// <summary>
        /// 行程介绍
        /// </summary>
        public string Jouneyintroduce { get; set; }
        /// <summary>
        /// 参考酒店
        /// </summary>
        public string Referenceintroduce { get; set; }
        /// <summary>
        /// 餐食
        /// </summary>
        public string meal { get; set; }
    }
}
