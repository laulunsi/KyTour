﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 机票表
    /// </summary>
    public class airticket
    {
        /// <summary>
        /// 机票id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 航班名
        /// </summary>
        public string Flightname { get; set; }
        /// <summary>
        /// 座位
        /// </summary>
        public string seat { get; set; }
        /// <summary>
        /// 折扣
        /// </summary>
        public string Discount { get; set; }
        /// <summary>
        /// 机场外键id
        /// </summary>
        public int Airportid { get; set; }
        /// <summary>
        /// 起飞时间
        /// </summary>
        public DateTime Takeofftime { get; set; }
        /// <summary>
        /// 抵达时间
        /// </summary>
        public DateTime Arrivaltiem { get; set; }
        /// <summary>
        /// 经停
        /// </summary>
        public int Stop { get; set; }
        /// <summary>
        /// 是否有餐饮
        /// </summary>
        public string yeornocaterint { get; set; }
    }
}
