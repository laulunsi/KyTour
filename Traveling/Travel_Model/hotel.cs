﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 酒店表
    /// </summary>
    public class hotel
    {
        /// <summary>
        /// 酒店编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 酒店地址
        /// </summary>
        public string hoteladdress { get; set; }
        /// <summary>
        /// 酒店名称
        /// </summary>
        public string hotelname { get; set; }
        /// <summary>
        /// 入住时间
        /// </summary>
        public string intime { get; set; }
        /// <summary>
        /// 离店时间
        /// </summary>
        public string outtime { get; set; }
        /// <summary>
        /// 酒店图片
        /// </summary>
        public string hotelImg { get; set; }
        /// <summary>
        /// 酒店星级
        /// </summary>
        public string hotelrank { get; set; }
        /// <summary>
        /// 酒店介绍
        /// </summary>
        public string hotelintroduce { get; set; }
    }
}
