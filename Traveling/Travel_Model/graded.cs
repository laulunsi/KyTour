﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 度假评分表
    /// </summary>
    public class graded
    {
        /// <summary>
        /// 评分id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 度假评分
        /// </summary>
        public string grade { get; set; }
        /// <summary>
        /// 度假外键id
        /// </summary>
        public int Holidayid { set; get; } 
    }
}
