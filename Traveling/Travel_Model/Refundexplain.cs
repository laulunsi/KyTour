﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 退款说明表
    /// </summary>
    public class Refundexplain
    {
        /// <summary>
        /// 退款id
        /// </summary>
        public int Refundexplainid { get; set; }
        /// <summary>
        /// 退款说明
        /// </summary>
        public string Refundexplains { get; set; }
    }
}
