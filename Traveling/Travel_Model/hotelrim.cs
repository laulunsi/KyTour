﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 酒店周边表
    /// </summary>
    public class hotelrim
    {
        /// <summary>
        /// 周边编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 餐饮
        /// </summary>
        public string Catering { get; set; }
        /// <summary>
        /// 购物
        /// </summary>
        public string Shopping { get; set; }
        /// <summary>
        /// 娱乐
        /// </summary>
        public string Entertainment { get; set; }
        /// <summary>
        /// 地铁站
        /// </summary>
        public string Subway { get; set; }
        /// <summary>
        /// 酒店外键id
        /// </summary>
        public int Hotelid { get; set; }
    }
}
