﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 个人信息表
    /// </summary>
    public  class Information
    {
        /// <summary>
        /// 乘客id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 乘客姓名
        /// </summary>
        public string Informationname { get; set; }
        /// <summary>
        /// 乘客类型
        /// </summary>
        public string InformationType { get; set; }
        /// <summary>
        /// 身份证信息
        /// </summary>
        public string IdInformation { get; set; }
        /// <summary>
        /// 身份证
        /// </summary>
        public string IDCrad { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        public string Captcha { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// 机票编号
        /// </summary>
        public int InformationId { get; set; }
        /// <summary>
        /// 价格编号
        /// </summary>
        public int PriceId { get; set; }


    }
}
