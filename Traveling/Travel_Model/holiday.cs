﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
  /// <summary>
    /// 度假表
    /// </summary>
    public  class holiday
    {
        
        /// <summary>
        /// 度假编号
        /// </summary>
        public int Holidayid { get; set; }
        /// <summary>
        /// 度假图片
        /// </summary>
        public string Hoildayimg { get; set; } 
        /// <summary>
        /// 套餐名
        /// </summary>
        public string Hoildaycomdo { get; set; }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string Hoildayproductid { get; set; }
        /// <summary>
        /// 度假价格
        /// </summary>
        public decimal Holidayprice { get; set; }
        /// <summary>
        /// 阅览人数
        /// </summary>
        public int Readnumber { get; set; }
        /// <summary>
        /// 出游日期
        /// </summary>
        public string Traveldate { get; set; }
        /// <summary>
        /// 行程天数
        /// </summary>
        public int Travelday { get; set; }
        /// <summary>
        /// 游玩路线
        /// </summary>
        public string Playline { get; set; }
        /// <summary>
        /// 路程特色
        /// </summary>
        public string Journeytese { get; set; }
        /// <summary>
        /// 费用说明
        /// </summary>
        public int Coitexplainid { get; set; }  
        /// <summary>
        /// 订单说明
        /// </summary>
        public int Ordernotictid { get; set; }
        /// <summary>
        /// 签证说明
        /// </summary>
        public int Visaexplainid { get; set; }
        /// <summary>
        /// 退款说明
        /// </summary>
        public int Refundexplainid { get; set; }
        /// <summary>
        /// 补充说明
        /// </summary>
        public int Elseexplainid { get; set; }

        /// <summary>
        /// 费用说明
        /// </summary>
        public string Coitexplain { get; set; }

        /// <summary>
        /// 服务信息
        /// </summary>
        public string Ordernotict { get; set; }

        /// <summary>
        /// 产品说明
        /// </summary>
        public string Visaexplain { get; set; }

        /// <summary>
        /// 违约说明
        /// </summary>
        public string  Refundexplain { get; set; }

        /// <summary>
        /// 温馨提示 
        /// </summary>
        public string Elseexplain { get; set; }


          


    }
}
