﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 价格表
    /// </summary>
    public class Price
    {
        /// <summary>
        /// 价格id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 成人价格
        /// </summary>
        public string Adult { get; set; }
        /// <summary>
        /// 儿童价格
        /// </summary>
        public string Child { get; set; }
        /// <summary>
        /// 机建费
        /// </summary>
        public string Airrax { get; set; }
        /// <summary>
        /// 燃油税
        /// </summary>
        public string Tax { get; set; }
        /// <summary>
        /// 保险费
        /// </summary>
        public string Insurance { get; set; }
    }
}
