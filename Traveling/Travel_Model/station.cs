﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 车站表
    /// </summary>
    public class station
    {
        /// <summary>
        /// 车站编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 起始站
        /// </summary>
        public string startstation { get; set; }
        /// <summary>
        /// 终点站
        /// </summary>
        public string Endstation { get; set; }
        /// <summary>
        /// 城市外键id
        /// </summary>
        public int cityid { get; set; }
    }
}
