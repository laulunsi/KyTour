﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 行程天数
    /// </summary>
    public class journeyday
    {
        /// <summary>
        /// 行程天数id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 行程天数
        /// </summary>
        public string Jday { get; set; }  
        /// <summary>
        /// 度假外外键id
        /// </summary>
        public int Holidayid { get; set; }
    }
}
