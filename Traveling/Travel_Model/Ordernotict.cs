﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 订单须知表
    /// </summary>
    public class Ordernotict
    {
        /// <summary>
        /// 订单须知id
        /// </summary>
        public int Ordernotictid { get; set; }
        /// <summary>
        /// 订单须知
        /// </summary>
        public string Ordernoticts { get; set; }  
    }
}
