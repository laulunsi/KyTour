﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 费用说明表
    /// </summary>
    public class Coitexplai
    {
        /// <summary>
        /// 费用说明id
        /// </summary>
        public int Coitexplainid { get; set; }
        /// <summary>
        /// 费用说明
        /// </summary>
        public string Coitexplain { get; set; }
    }
}
