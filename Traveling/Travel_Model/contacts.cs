﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 
    /// </summary>
    public class contacts
    {
        /// <summary>
        /// 联系人编号
        /// </summary>
        public int contactsid { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string ContName { get; set; }
        /// <summary>
        /// 联系人手机号
        /// </summary>
        public string Contactway { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Mailbox { get; set; }
    }
}
