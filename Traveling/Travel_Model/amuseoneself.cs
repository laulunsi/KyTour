﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 游玩人类别
    /// </summary>
    public class amuseoneself
    {
        /// <summary>
        /// 游玩人类别id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 游玩人类别
        /// </summary>
        public string amuseType { get; set; }
        /// <summary>
        /// 游玩人数量
        /// </summary>
        public int amuseNum { get; set; }
        /// <summary>
        /// 度假外键id
        /// </summary>
        public int Holidayid { get; set; }
    }
}
