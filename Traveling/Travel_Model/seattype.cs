﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    ///座位类型表
    /// </summary>
    public class seattype
    {
        /// <summary>
        /// 类别编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 类别名称
        /// </summary>
        public string seattypename { get; set; }
        /// <summary>
        /// 参考票价
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 车次外键id
        /// </summary>
        public int Trainnumberid { get; set; }
    }
}
