﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    ///评分表
    /// </summary>
    public class grade
    {
        /// <summary>
        /// 评分id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 酒店评分
        /// </summary>
        public string Hotelgrade { get; set; }
        /// <summary>
        /// 酒店外键id
        /// </summary>
        public int hotelid { get; set; }
    }
}
