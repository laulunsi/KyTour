﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 床类型表
    /// </summary>
    public class bed
    {
        /// <summary>
        /// 床铺编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 床类型
        /// </summary>
        public string Bedtype { get; set; }
        /// <summary>
        /// 是否有早餐
        /// </summary>
        public string Breakfast { get; set; }
        /// <summary>
        /// 有无宽带
        /// </summary>
        public string Broadband { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 酒店外键
        /// </summary>
        public int Hotelid { get; set; }
    }
}
