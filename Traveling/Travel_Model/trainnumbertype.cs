﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 车次类别表
    /// </summary>
    public class trainnumbertype
    {
        /// <summary>
        /// 类别编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 车次id
        /// </summary>
        public int Trainnumberid { get; set; }
        /// <summary>
        /// 车次类别
        /// </summary>
        public string Traintype { get; set; }
    }
}
