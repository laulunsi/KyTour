﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
   public  class user_type
    {
       /// <summary>
       /// 类型id
       /// </summary>
       public int usertype { get; set; }
       /// <summary>
       /// 类型名称
       /// </summary>
       public string  typename { get; set; }
    }
}
