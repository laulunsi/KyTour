﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 城市表
    /// </summary>
    public class city
    {
        /// <summary>
        /// 城市编号
        /// </summary>
        public int cityid { get; set; } 
        /// <summary>
        /// 城市名称
        /// </summary>
        public string Cityname { get; set; }
    }
}
