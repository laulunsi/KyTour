﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 机场表
    /// </summary>
    public class airport
    {
        /// <summary>
        /// 机场id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 起飞机场
        /// </summary>
        public string Startairport { get; set; }
        /// <summary>
        /// 抵达机场
        /// </summary>
        public string Endairport { get; set; }
        /// <summary>
        /// 机场图片
        /// </summary>
        public string Photo { set; get; }
        /// <summary>
        /// 航空公司外键id
        /// </summary>
        public int Airlineid { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string Takeofftime { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Adult { get; set; }

    }
}
