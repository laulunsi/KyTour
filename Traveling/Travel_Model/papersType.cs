﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 证件类别表
    /// </summary>
    public class papersType
    {
        /// <summary>
        /// 证件类别编号
        /// </summary>
        public int pid { get; set; }
        /// <summary>
        /// 证件类别名称
        /// </summary>
        public string PaperName { get; set; }
    }
}
