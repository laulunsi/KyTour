﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 用户表
    /// </summary>
    public class userinfor
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        public int uid { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string userpwd { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string userphone { get; set; }
        /// <summary>
        /// 手机验证码
        /// </summary>
        public string trendsnumber { get; set; }
        /// <summary>
        /// 账号类别
        /// </summary>
        public int usertype { get; set; }
        /// <summary>
        /// 用户类型
        /// </summary>
        public string  typename { get; set; }
    }
}
