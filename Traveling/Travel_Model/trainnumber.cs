﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 车次表
    /// </summary>
    public class trainnumber
    {
        /// <summary>
        /// 车次编号
        /// </summary>
        public int Trainnumberid { get; set; }
        /// <summary>
        /// 车次名称
        /// </summary>
        public string Trainname { get; set; }
        /// <summary>
        /// 出发时间
        /// </summary>
        public string Starttime { get; set; }
        /// <summary>
        /// 抵达时间    
        /// </summary>
        public string Endtime { get; set; }
        /// <summary>
        /// 经停站
        /// </summary>
        public string Stop { get; set; }
        /// <summary>
        /// 经停时间
        /// </summary>
        public string Stoptime { get; set; }
        /// <summary>
        /// 运行时间
        /// </summary>
        public string operation { get; set; }
        /// <summary>
        /// 车票剩余数量
        /// </summary>
        public int Farenumber { get; set; }





        
        /// </summary>
        public string seattypename { get; set; }
        /// <summary>
        /// 参考票价
        /// </summary>
        public string Price { get; set; }


        public string  startstation { get; set; }
        public string Endstation { get; set; }


        
        /// <summary>
        /// 车次类别
        /// </summary>
        public string Traintype { get; set; }
    }
}
