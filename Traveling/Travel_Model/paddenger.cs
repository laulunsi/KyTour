﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 乘客信息表
    /// </summary>
    public class paddenger
    {
        /// <summary>
        /// 乘客编号
        /// </summary>
        public int paddengerid { get; set; }
        /// <summary>
        /// 成人或者儿童
        /// </summary>
        public string agegrades { get; set; }
        /// <summary>
        /// 中文姓名
        /// </summary>
        public string PaddName { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Contactway{get;set;}
        /// <summary>
        /// 证件类别编号
        /// </summary>
        public int pid { get; set; }
        /// <summary>
        /// 证件号码
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 是否买保险
        /// </summary>
        public string Whether { get; set; }



        /// <summary>
        /// 联系人编号
        /// </summary>
        public int contactsid { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string ContName { get; set; }
        /// <summary>
        /// 联系人手机号
        /// </summary>
        public string Contactway { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Mailbox { get; set; }





        /// <summary>
        /// 证件类别名称
        /// </summary>
        public string PaperName { get; set; }
    }
}
