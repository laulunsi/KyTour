﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Travel_Model
{
    /// <summary>
    /// 房间表
    /// </summary>
    public class room
    {
        /// <summary>
        /// 房间编号
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 房间类型
        /// </summary>
        public string Roomtype { get; set; }
        /// <summary>
        /// 酒店外键id
        /// </summary>
        public int Hotelid { get; set; }
    }
}
