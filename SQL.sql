
use Traveling

--2016年11月29赵志强创建用户表
if object_id('userinfor') is null
  create table userinfor
  (
	uid int primary key identity,      --用户编号
	username nvarchar(50) not null,    --用户名
	userpwd nvarchar(50) not null,     --密码
	userphone nvarchar(11) not null,   --电话
	trendsnumber nvarchar(10) not null,--手机验证码
	usertype int not null              --账号类别
  )
---2016年11月29赵志强添加用户表的测试数据
insert into userinfor(username,userpwd,userphone,trendsnumber,usertype) values
('admin','admin','12345678910','1234',0),('123','123','12345678910','4321',1)
  select * from userinfor
  
--2016年11月29赵志强创建用户类型表并添加两条固定数据
if OBJECT_ID('user_type') is null
create table user_type
(
	usertype int primary key  ,       --类别ID
	typename nvarchar(30) not null    --类别名称
)

--2016年11月29赵志强添加用户类型测试数据（先查询看有没有数据 如果有两条了就不用再执行添加语句了，如果没有再执行下面的语句）
--insert into user_type(usertype,typename) values(0,'管理员'),(1,'用户')
select * from user_type

--2016年11月29赵志强创建广告图片轮播表：advertisement
if OBJECT_ID('advertisement') is null
create  table  advertisement
(
	id int primary key identity not null,--轮播编号
	Imgurl varchar(200) not null,        --图片地址
	url varchar(200)                     --跳转地址
)
--2016年11月29日赵志强添加轮播图片测试数据
insert into advertisement(Imgurl,url) values
('../Content/img/img/silder_img101.jpg',''),
('../Content/img/img/silder_img102.jpg',''),
('../Content/img/img/silder_img103.jpg','')
select * from advertisement

--2016年11月29赵志强创建导航表
if OBJECT_ID('nav') is null
create  table  nav
(
	id int primary key identity not null,--导航编号
	navname varchar(30) not null,        --导航名称
	navurl varchar(200)                  --导航路径
)
---2016年11月29赵志强添加导航表测试数据
insert into nav(navname,navurl) values('首页','HtmlPage1.html'),('酒店','Hotel.html'),('机票','AirTicket.html'),('度假','Holiday.html'),('签证',''),('火车票','TrainTicket.html')
select * from nav






--季云龙（2016/11/29  晚上）

use Traveling

if  OBJECT_ID('Airline') is null		--航空公司表
create table Airline
(
   id  int primary key identity,		--航空公司id
   Airlinename  nvarchar(50),			--航空公司名称
)
go
--添加航空公司
insert into  Airline values('吉祥航空公司'),('南方航空公司'),('东方航空公司'),('国际航空公司')
--查看
select * from  Airline



if  OBJECT_ID('airport') is null		--机场表
create table airport
(
   id  int primary key identity,		--机场id
   Startairport  nvarchar(50),			--起飞机场
   Endairport nvarchar(50),				--抵达机场
   Airlineid int						--航空公司id
)
go
--添加机场
insert into airport values('北京首都机场T3','上海虹桥机场T2',1),
                          ('北京首都机场T2','上海虹桥机场T2',2),
                          ('北京首都机场T2','上海虹桥机场T2',3),
                          ('北京首都机场T3','上海虹桥机场T2',4)
--查看
select * from airport

   
if  OBJECT_ID('airticket') is null		--机票表
create table airticket
(
   id  int primary key identity,		--机票id
   Flightname  nvarchar(50),			--航班名
   seat nvarchar(50),					--座位
   Discount nvarchar(50),				--折扣
   Airportid int,						--机场ID
   Takeofftime datetime,				--起飞时间
   Arrivaltiem   datetime,				--抵达时间
   Stop int ,							--经停
   yeornocaterint nvarchar(50)			--是否有餐饮
)
go

--添加机票
insert into  airticket values('吉祥航空HO1252','经济舱(Q)','4折','1','06:35','08:50','0','有餐'),
                             ('南方航空CZ6412','经济舱(V)','4.6折','2','06:40','09:00','0','有餐'),
                             ('东方航空MU5138','经济舱(S)','5.1折','3','07:00','09:15','0','有餐'),
                             ('国际航空CA1835','经济舱(W)','5.1折','4','07:55','10:15','0','有餐')
--查看机票
select * from  airticket

if  OBJECT_ID('Information') is null	--个人信息表
create table Information
(
   id  int primary key identity,		--乘客id
   Informationname  nvarchar(50),		--乘客姓名
   InformationType nvarchar(50),		--乘客类型
   IdInformation nvarchar(50),			--身份证信息
   IDCrad nvarchar(50),					--身份证
   Phone nvarchar(50),					--手机号
   Captcha  nvarchar(50),				--验证码
   email nvarchar(50) ,					--电子邮箱
   InformationId int,					--机票编号
   PriceId int							--价格编号
)
go
--添加个人信息
insert into Information values('张三','成人','二代身份证','232103199808088888','12345678901','4567','qwqe@qq.com','1','1'),
                              ('小李','成人','二代身份证','232103199808088812','12345678222','4587','qwqedsad@qq.com','2','1'),
                              ('小明','儿童','二代身份证','232103199808081111','12345673333','4467','sadasd@qq.com','3','2'),
                              ('万五','成人','二代身份证','23210319980805555','12345676666','4789','dsadfg@qq.com','4','1')
--查看
select * from  Information     

if  OBJECT_ID('Price') is null			--价格表
create table Price
(
   id  int primary key identity,		--价格id
   Adult  nvarchar(50),					--成人价格
   Child nvarchar(50),					--儿童价格
   Airrax nvarchar(50),					--机建费
   Tax nvarchar(50),					--燃油税
   Insurance nvarchar(50)			    --保险费
  
)
go
--添加价格
insert into Price values('￥680x1人','￥300x1人','￥50/1人','￥0/1人','￥20')
--查看
select * from Price







--2016/11/29  翟培杰
use Traveling
if object_id('Coitexplain') is null
--费用说明表
  create table Coitexplain
  (
	Coitexplainid int primary key identity, --费用说明id
	Coitexplain nvarchar(500)-- 费用说明
  )
  go
  if object_id('Ordernotict') is null
  --订单须知表
  create table Ordernotict
  (
	Ordernotictid int primary key identity,--订单须知ID
	Ordernotict nvarchar(1000)--订单须知
  )
  go
  if object_id('Visaexplain') is null
  --签证说明表
  create table Visaexplain
  (
	Visaexplainid int primary key identity,--签证说明ID
	Visaexplain nvarchar(500)--签证说明
  )
  go
   if object_id('Refundexplain') is null
  --退款说明表
  create table Refundexplain
  (
	Refundexplainid int primary key identity,--退款说明ID
	Refundexplain nvarchar(500)--退款说明
  )
  go
     if object_id('Elseexplain') is null
  --补充说明表
  create table Elseexplain
  (
	Elseexplainid int primary key identity,--补充说明ID
	Elseexplain nvarchar(3000)--补充说明
  )
--度假表
go
 if object_id('Holiday') is null
  create table Holiday
  (
	Holidayid int primary key identity,--度假编号
	Holidayimg nvarchar(500) not null,--度假图片
	Holidaycomdo nvarchar(500) not null,--套餐名
	Holidayproductid nvarchar(500) not null,--产品编号
	Holidayprice money not null,--度假价格
	Readnumber int not null,--阅览人数
	Traveldate datetime not null,--出游日期
	Travelday int not null,--行程天数
	Playline nvarchar(500) not null,--有玩路线
	Journeytese nvarchar(500) not null,--路程特色
	Coitexplainid int not null,--费用说明
	Ordernotctid int not null,--订单说明
	Visaexplainid int not null,--签证说明
	Refundexplainid int not null,--退款说明
	Elseexplainid int not null	--补充说明
  )
go
if object_id('Graded') is null
--度假评分表
create table Graded  
(
	id int primary key identity,--评分ID
	Greade nvarchar(500),	--度假评分
	Holidayid int not null --度假外键id
)
go
if object_id('Amuseoneself') is null
--游玩人类别
create table Amuseoneself
(
	id int primary key identity,--游玩人类别ID
	amuseType nvarchar(500) not null,--游玩人类别
	amuseNum int  not null,--游玩人数量
	Holidayid int not null  --度假ID
)
go
if object_id('Journeyreferral') is null
--行程介绍
create table Journeyreferral
(
	id int primary key identity,--行程介绍ID
	Jouneyegion nvarchar(500) not null,--行程地区
	Traffcvehicle nvarchar(500) not null,--交通工具
	Jouneyintroduce nvarchar(1500) not null,--行程介绍
	Referenceintroduce nvarchar(500) not null,--参考酒店
	meal nvarchar(500) not null--餐食
)
go
--行程天数
if object_id('journeyday') is null
create table journeyday
(
	id int primary key identity, --行程天数ID
	journeyday nvarchar(500) not null,--行程天数
	Holidayid int not null --度假ID
)
go
--2016/11/29 
--测试数据
insert into Coitexplain values('以上为参考行程，具体报价和团期请联系客服；
费用包含
北京/甲米/普吉岛往返国际机票，团队经济舱，含机场建设税；
1、 5晚星级酒店
2、 境外旅游巴士及外籍司机；（根据团队人数，通常为20-50座）
3、 行程所列中餐晚餐（8菜一汤），8-10人一桌；')
select * from Coitexplain
go
insert into Ordernotict values('服务标准
如遇部分景点节假日休息或庆典等，本社有权根据实际情况调整行程游览先后顺序，以尽可能保证游览内容，但客客观因素限制确实无法安排的，本社将根据实际情况进行调整，敬请各位贵宾理解与配合！
根据国际航班团队搭乘要求，团队通常须提前3-3.5小时到达机场办理登机手续，故国际段航班在当地下午15点前（含15点），晚间21点前（含21点）起飞的，行程均不含午餐或晚餐
出于安全考虑和泰国有关方面的要求，参团的客人必须在白天按照行程活动，如果在出团前没有声明在境外白天将要脱离团队不按照行程活动，而在境外发生此类情况，泰国接待方将要报警备案；
行程内赠送游览项目，以增长见识，开阔视野为目的。导游可根据天气，当地交通等行程进行调整。如因天气、交通等不可抗力导致不能乘坐快艇，则改为乘大船前往大PP岛往返，中途各项（帝王岛、小PP岛环岛 情人沙滩、国家珊瑚保护区、热带鱼保护区、珍珠湾、燕窝洞）将不能前往。如客人自行放弃行程中赠送项目（人妖、精油Spa等不适宜儿童的），不可退费；
包机（或者其它航班）有可能根据起飞当日情况调整起飞时间及候机楼或登机口，此类情况请以出团确认和办理登机手续时的信息为准
请旅客携带中国境内的银联卡或信用卡，以备万一之需。因不可抗拒的客观原因和非旅行社原因（如天灾、战争、罢工、政府行为等）或航空公司航班延误或取消、使领馆签证延误、报名人数不足等特殊情况，我公司有权取消或变更行程，一切超出费用（如在外延期签证费、住、食及交通费、国家航空运价调整等）由旅客在境外自理付费，旅行社有权追加差价')
select * from Ordernotict
go
insert into Visaexplain values('本产品需要办理签证，需您提前办理。详情请联系你定网客服专线400-001-7171转出境旅游。')
select * from Visaexplain
go
insert into Refundexplain values('1、旅行社违约：
行程前150日至31日，退还全额旅游费用，支付旅游费用总额50％的违约金。
行程前30日至26日，退还全额旅游费用，支付旅游费用总额55％的违约金。
行程前25日至16日，退还全额旅游费用，支付旅游费用总额60％的违约金。
行程前15日至7日，退还全额旅游费用，支付旅游费用总额65％的违约金。
行程前6日至1日，退还全额旅游费用，支付旅游费用总额70％的违约金。
行程开始当日，退还全额旅游费用，支付旅游费用总额70％的违约金。2、在行程前解除合同的，必要的费用扣除标准为：
行程前150日至31日，收取旅游费用总额80％的违约金。
行程前30日至26日，收取旅游费用总额85％的违约金。
行程前25日至16日，收取旅游费用总额90％的违约金。
行程前15日至7日，收取旅游费用总额95％的违约金。
行程前6日至1日，收取旅游费用总额100％的违约金。
行程开始当日，收取旅游费用总额100％的违约金。')
select * from Refundexplain
delete  from Refundexplain where Refundexplainid='2'
go
insert into Elseexplain values('温馨提示
如持非中华人民共和国护照的客人在国内参团往返，请务必自行确认是否免签及跟团出境后团队返回时能再次入中国境内；如因客人原因不能出入境的，损失由客人自理；
注意事项
1、安全事宜
境外游览时请注意人身安全和财产安全。整体治安相对较好，但某些国家也存在治安隐患，尤其景区、酒店大堂、百货公司、餐厅等游客聚集的地方更是偷窃行为多发地，请游客务必随同导游带领并注意结伴而行，在游玩过程中，时刻注意自己随身携带的物品安全；
乘坐交通工具时，现金、证件或贵重物品请务必随身携带，不应放进托运行李内；外出旅游离开酒店及旅游车时，也请务必将现金、证件或贵重物品随身携带。因为酒店不负责客人在客房中贵重物品安全，司机也不负责巴士上旅客贵重物品的安全，保险公司对现金是不投保的。
2、货币兑换及时差
在泰国使用货币为泰铢为主，人民币与泰铢的比值是1：4左右，泰铢在泰国机场或银行均可换取，建议和导游兑换，这样离境时可以用相同汇率换回人民币，另建议携带一张国际信用卡出行；
泰国比北京时间晚1小时，如北京时间为10:00，则泰国时间为09:00。
3、风险提示
游客需对其本人身体状况是否适合本次旅游负责，另外应加强安全防范意识、服从安排、听从劝告。特别是在海边、泳池游泳、潜水、漂流等从事危险活动时，应根据自身身体情况决定运动时间、运动量，避免意外溺水等意外事件的发生，以确保您自身的人身安全问题，避免发生意外事故；
旅行社对于游客参加活动时因个人因素和不可抗力因素以及第三方原因造成的事故和伤害不承担法律、经济和医疗责任；游客承诺如因个人因素和不可抗力因素以及第三方原因造成的事故和伤害，将不追究旅行社的任何民事、经济和医疗责任；另外，根据旅游行业惯例和旅游合同之规定，旅行社对于游客在自由活动期间所发生的意外事件不承担责任，如游客在自由活动期间欲从事有任何危险因素的活动时（如游泳、潜水、漂流等），一定要注意安全，在不能确定是否安全时，最好不要从事此类活动；
旅游期间财物请随身保管，车上不可放贵重物品，自由活动期间注意安全，证件最好交酒店保险箱寄存；如已发生意外事故，请游客及时与领队联系，以方便旅行社组织救援或调整计划，并协调配合处理相关事件。如因不听从劝告擅自从事危险活动（如游泳等）及自由活动期间发生意外事故，责任由旅行者自行负责。
旅行社原则上不会在行程中安排高风险项目，在自由活动期间请您慎重选择参加诸如吉普车越野、摩托车驾驶、自驾车、蹦极、水上摩托、滑翔伞、冲浪、探险等高风险项目；
A. 行程中有些项目如赠送的快艇套餐等是较刺激的项目，请客人在乘坐时听从导游、领队安排，穿好救生衣，尽量坐在快艇中后部，坐稳扶好，否则在快艇行驶过程中可能会由于颠簸而导致腰部、尾椎等受伤。待快艇停稳后方可上、下艇；
B. 游泳及潜水入水前请确认身体状况适合游泳，没有影响身体机能的慢性病及心脏病，入水前严禁暴饮暴食及空腹下水。杜绝独身下水，游泳时应有身强体壮或有照应能力的同伴跟随；
C. 坚决不要到离岸较远的非游泳区游泳，海中会有海蜇、珊瑚等危险动物，以免受伤；
D. 如在境外参加潜水、浮潜等活动前，请确保自身会游泳及会使用潜水设备，如遇外籍教练进行活动前培训，应确保能够正确理解教练的一切意图，否则不要参加此项活动；
F. 有心脑血管疾病、慢性病及特殊疾病史的客人（尤其腰椎病史）、老人和儿童谨慎参加，18岁以下未成年人需在监护人的陪同下参加，孕妇坚决不能参加浮潜、快艇等此类项目；
G. 如遇天气不好，雨天、大风大浪等情况一定不要冒险下水、乘坐快艇；
H. 坚决不参加吉普车越野、摩托车驾驶、自驾车、水上摩托、滑翔伞等高风险项目；
如已发生意外事故，请游客及时与领队联系，以方便旅行社组织救援或调整计划，并协调配合处理相关事件。如因不听从劝告擅自从事危险活动（如游泳等）及自由活动期间发生意外事故，责任由旅行者自行负责；')
select * from Elseexplain
go
insert into Holiday values('Traveling/Content/img/img/20161201174945620_295_347.jpg','[观光]甲如有米普吉甲米5晚7天','100305','6080 ','20','2016-11-29','7','北京→曼谷→普吉→普吉岛→甲米','  甲米1晚当五 普吉1晚温泉五星级酒店（温泉海滩度假酒店）或同级含天然海底温泉，迷你高尔夫竞技，逍遥脚踏车，不一般的感受.','1','1','1','1','1' )
select * from Holiday

go
insert into Graded values('5')
select * from Graded
go
insert into Amuseoneself values('成人','3','1')
select * from Amuseoneself
go
insert into Journeyreferral values('普吉岛-',' 巴士',' 
酒店 普吉攀牙湾 割喉岛泛舟 007岛(远眺) 沙法里四合一
早餐后，您可以享受1..天然海底温泉，活化细胞达到美容养生效果。2.冰水池，结合温泉阴阳调理。3.迷你高尔夫：亲子、情侣、好友、伙伴来一场欢乐无穷竞赛 ；4.逍遥脚踏车；5.海边沙滩漫步：沙滩上踏着轻快的步履，畅游一番，乐不思蜀；6.桌球：与小伙伴们来一场精彩的比赛；7.健身房：各式健身器随您选。这些可都是免费享用！于指定时间 前往码头搭船漫游有海上小桂林美誉的【攀牙湾】（约1小时30分种）。由石灰岩所组成的大小海岛星罗棋布；随即续前往因电影【割喉岛】而闻名于世之拍片地点，特别赠送乘坐【橡皮艇】（约1小时）让您展开一段惊奇的知性之旅，巡游经千万年海水冲击形成的各种岩洞及钟乳石洞，映入眼帘尽是浑然天成的自然景观，美不胜收。之后远眺[007大战金枪客]的拍摄地[占士帮007岛]。餐后来趟大自然之旅【沙法里四合一】（约1小时）首先前往从林探险活动，让您【骑乘大象】上，轻松漫步于树林、沼泽中；随后体验【鱼疗足浴】君置于玻璃柜中亲亲鱼会主动围上来亲吻，灵巧的小嘴为远道而来的客人驱走疲劳，令您精神充沛，更能让您体验一种难于用语言所描述的奇妙之感。随后安排前往【猴子学校】参观令人捧腹，拍案叫绝精彩表演，再前往橡胶园观看【割橡胶】过程，让您轻松愉快享受悠闲渡假时光。','普吉国际五星酒店','【早餐:酒店内】,【午餐:回教村海鲜餐厅】,【晚餐:火锅餐】')
select * from Journeyreferral
go
insert into journeyday values('7','1')
select * from journeyday




--备注：张鹤腾
--时间：2016年11月29日

--使用数据库
use Traveling
--创建酒店表
if object_id('hotel') is null
create table hotel
(
	id	Int	primary key identity,			--酒店编号
	hoteladdress Nvarchar(50),              --酒店地址
	hotelname Nvarchar(50),                 --酒店名称
	intime Nvarchar(50),                    --入住时间
	outtime Nvarchar(50),                   --离店时间
	hotelImg Nvarchar(50),                  --酒店图片
	hotelrank Nvarchar(50),                 --酒店星级
	hotelintroduce Nvarchar(50)             --酒店介绍
)
go

--创建评分表
if object_id('grade') is null
create table grade
(
	id int primary key identity,		--评分编号
	Hotelgrade Nvarchar(50),			--酒店评分
	hotelid int							--酒店ID
)
go

--创建酒店信息
if object_id('hotelmessage') is null
create table hotelmessage
(
	id Int primary key identity,		   --信息编号
	basicmessage Nvarchar(50),			   --基本信息
	Hotelfacility Nvarchar(50),			   --酒店设施
	Hotel Int							   --酒店ID
)
go

--创建酒店周边表
if object_id('hotelrim') is null
create table hotelrim
(
	id Int primary key identity,			--周边编号
	Catering Nvarchar(50),					--餐饮
	Shopping Nvarchar(50),					--购物
	Entertainment Nvarchar(50),				--娱乐
	Subway Nvarchar(50),					--地铁站
	Hotelid int								--酒店外键
)
go

--创建房间表
if object_id('room') is null
create table room
(
	id Int primary key identity,			--房间编号
	Roomtype Nvarchar(50),					--房间类型
	Hotelid int								--酒店外键
)
go

--创建床类型表
if object_id('bed') is null
create table bed
(
	id Int primary key identity,			--床铺编号
	Bedtype Nvarchar(50),					--床类型
	Breakfast bit,							--是否早餐
	Broadband bit,							--有无宽带
	Price Money,							--价格
	Hotelid Int								--酒店外键
)
go


--添加测试数据
insert into hotel values('青岛 市南区 彰化路2号 ，近东海路','青岛海景花园大酒店','2016-11-29','2016-11-30','','五','该酒店是一家欧陆庭院式建筑风格的五星级酒店，地处海滨，面朝大海，四周围绕着美丽的园景花园，与大 ')
insert into grade values('4.8','1')
insert into hotelmessage values('1995年开业 2014年装修 376间房 ','收费wifi、免费停车场、室外游泳池、健身室、会议厅、餐厅','1')
insert into hotelrim values('钻石俱乐部中餐厅、夏威夷烧烤、粤海中餐厅','海景花园便利超市、百丽广场、海信广场、佳世客超市','蓝吧、海景花园桑拿洗浴中心','13号线','1')
insert into room values('豪华北向标准房','1')
insert into bed values('双床','true','false','997','1')
--查询数据
select * from bed

--2016年11月29日   张泽宇     火车票模板表
use Traveling
--创建座位类型表时判断座位类型表是否存在
if object_id('seattype') is null
create table seattype
(
	seattypeid  Int primary key identity ,--类别编号
	seattypename Nvarchar(50) not null,--类别名称
	Price money  not null,--参考票价
	Trainnumberid int not null--车次ID
)
go
insert into seattype values
('特等座','0',1),
('商务座','1748.0',1),
('一等座','933.0',1),
('二等座','553.0',1),
('高级软卧','0',1),
('软卧','0',1),
('硬卧','0',1),
('软座','0',1),
('硬座','0',1)
go
select * from seattype
go
--创建车次类别表时判断车次类别表是否存在
if object_id('trainnumbertype') is null
create table trainnumbertype
(
	trainnumbertypeid Int primary key identity ,--类别编号
	Trainnumberid int  not null,--车次ID
	Traintype Nvarchar(500) not null--车次类别
)
go
insert into trainnumbertype values
(1,'G-高铁'),
(3,'D-动车'),
(3,'T-特快'),
(3,'Z-直达'),
(1,'K-快速')
go
select * from trainnumbertype
go
--创建车次表时判断车次表是否存在
if object_id('trainnumber') is null
create table trainnumber
(
	Trainnumberid Int primary key identity ,--车次编号
	Trainname Nvarchar(20) not null,--车次名称
	Starttime datetime not null,--出发时间
	Endtime datetime not null,--抵达时间
	[Stop] Nvarchar(500) not null,--经停站
	Stoptime Nvarchar(500) not null,--经停时间
	operation Nvarchar(500) not null,--运行时间
	Farenumber int not null--车票剩余数量
)
go
insert into trainnumber values 
('G7','2016-11-29 19:00','2016-11-29 23:55','济南西、南京南','20:32，停2分、22:46，停2分','4小时55分，当日到达',0),
('T109','2016-11-29 19:33','2016-11-30 10:43','天津西、沧州、德州、徐州、南京、常州、无锡、苏州','20:55，停3分、21:55，停4分、23:05，停3分、03:12，停16分、06:59，停6分、08:38，停4分、09:05，停5分、09:39，停4分','15小时10分，次日到达',121),
('D313','2016-11-29 19:34','2016-11-30 07:41','南京、常州、苏州','04:51、停6分、05:58，停9分、06:46，停4分','12小时7分，次日到达',7),
('D321','2016-11-29 21:23','2016-11-30 09:13','南京、苏州','06:40，停4分、08:20，停2分','11小时50分，次日到达',0)
go
select * from trainnumber
go
--创建城市表时判断城市表是否存在
if object_id('city') is null
create table city
(
	cityid  Int primary key identity ,--城市编号
	Cityname Nvarchar(50) not null--城市名称
)
go
insert into city values
('北京'),
('上海'),
('广州')
go
select * from city
go
--创建车站表时判断车站表是否存在
if object_id('station') is null
create table station
(
	stationid Int primary key identity ,--车站编号
	startstation Nvarchar(50) not null,--起始站
	Endstation Nvarchar(50) not null,--终点站
	--cityid  int foreign key references city(cityid)--城市ID
)
go
insert into station  values
('北京南','上海虹桥')
go
select * from station
go
--创建时段表时判断时段表是否存在
if object_id('timeframe') is null
create table timeframe
(
	timeframeid Int primary key identity ,	--时段编号
	Timecontent Nvarchar(50) not null	--时段内容
)
go
insert into timeframe values
('0-6点'),
('6-12点'),
('12-18点'),
('18-24点 ')
go
select * from timeframe
go
--创建证件类别表时判断证件类别表是否存在
if object_id('papersType') is null
create table papersType
(
	pid Int primary key identity ,	--证件类别编号
	PaperName Nvarchar(50) not null	--证件类别名称
)
go
insert into papersType values
('身份证'),
('港澳通行证'),
('台湾通行证'),
('护照')
go
select * from papersType
go
--创建乘客信息表时判断乘客信息表是否存在
if object_id('paddenger') is null
create table paddenger
(
	paddengerid Int primary key identity ,	--乘客编号
	agegrades Nvarchar(50) not null,	--成人或者儿童
	PaddName Nvarchar(50) not null,	--中文姓名
	Contactway Nvarchar(50) not null,	--手机号码
	pid  int foreign key references papersType(pid),	--证件类别编号
	Number Nvarchar(50) not null,	--证件号码
	Whether Nvarchar(50) not null,	--是否买保险
)
go
insert into paddenger values
('成人','张三','12154211542',1,'131025199412253618','是') 
go
select * from paddenger
go
--创建联系人表时判断联系人表是否存在
if object_id('contacts') is null
create table contacts
(
	contactsid Int primary key identity ,	--联系人编号
	ContName Nvarchar(50) not null,	--联系人姓名
	Contactway Nvarchar(50) not null,	--联系人手机号
	Mailbox Nvarchar(50) not null	--邮箱
)
go
insert into contacts values 
('李四','121454512548','1215421554@163.com')
go
select * from contacts

--季云龙（2016/11/30）机场表新增一个字段photo
alter table airport add photo nvarchar(500)


--季云龙（2016/11/30）机场表photo添加图片路径
update airport set photo='/img/AirPortCity_CAN.jpg' where id=1
update airport set photo='/img/AirPortCity_CKG.jpg' where id=2
update airport set photo='/img/AirPortCity_CTU.jpg' where id=3
update airport set photo='/img/AirPortCity_PEK.jpg' where id=4

---赵志强 12.1 删除用户表中一个字段
ALTER TABLE userinfor DROP COLUMN trendsnumber


--季云龙（12/01）机票表添加一个字段
 alter table airticket add airticketprice money
--季云龙（12/01）机票表修改字段
update airticket set airticketprice='500' where id=1
update airticket set airticketprice='570' where id=2
update airticket set airticketprice='520' where id=3
update airticket set airticketprice='580' where id=4

